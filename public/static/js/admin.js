$(".btn-add").on('click', function (res) {
    var _data = $(this).data('action');
    $.ajax({
        url: _data,
        type: "get",
        dataType: 'json',
        success: function (data) {
            if (data.code === 0) {
                $.alert({
                    title: '',
                    type: 'red',
                    content: data.msg
                });
            }else{
                location.href = _data;
            }
        }
    });
});
$(function () {
    /**
     * 后台侧边菜单选中状态
     */
    //$('#nav-item').find('a').removeClass('active-link');
    $('.nav-tree').find('a[href*="'+GV.current_controller+'"]').parent().addClass('active-link').parents('#nav-item').addClass('active-sub');
    //console.log($('#nav-item').find('.collapse').attr('aria-expanded'));
    $('.active-sub').find('.collapse').addClass('in');
    /**
     * 清除缓存
     */
    $('#clear-cache').on('click', function (event) {
        event.preventDefault();
        var _url = $(this).data('url');
        if(_url !== 'undefined'){
            $.ajax({
                url: _url,
                success: function (data) {
                    if(data.code === 0){
                        setTimeout(function () {
                            location.href = location.pathname;
                        }, 100000);
                    }
                    $.alert({
                        title: '',
                        type: 'red',
                        content: data.msg
                    });
                }
            });
        }

        return false;
    });

    $("table").on('click', '.btn-msg', function (res) {
        bootbox.alert("该功能暂未开放");
    });
    $("table").on('click', '.btn-href', function (res) {
        var _data = $(this).data('action');
        $.ajax({
            url: _data,
            type: "get",
            dataType: 'json',
            success: function (data) {
                if (data.code === 0) {
                    $.alert({
                        title: '',
                        type: 'red',
                        content: data.msg
                    });
                }else{
                    location.href = _data;
                }
            }
        });
    });
    /**
     * 通用删除
     */
    $("table").on('click', '.ajax-delete', function () {
        var _href = $(this).attr('href');
        var _data = $(this).data('action');
        var url;
        if (_href == "" || _href == undefined) {
            url = _data;
        } else {
            url = _href;
        }
        $.confirm({
            title: '',
            content: '您确定要进行删除操作？<br/>删除后将不可恢复！',
            type: 'red',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'OK',
                    btnClass: 'btn-red',
                    action: function () {
                        $.ajax({
                            url: url,
                            type: "get",
                            dataType: 'json',
                            success: function (data) {
                                if (data.code === 0) {
                                    $.alert({
                                        title: '',
                                        type: 'red',
                                        content: data.msg
                                    });
                                }else{
                                    location.href = data.url;
                                }
                            }
                        });
                    }
                },
                close: function () {
                }
            }
        });
        return false;
    });

    /**
     * 通用的保存按钮提交
     */
    $('.btn-save').on('click', function () {
        var bt_obj = $('#button1');
        var fromId = bt_obj.parents('form').attr('id');
        var method = bt_obj.parents('form').attr("method");
        var url = bt_obj.parents('form').attr("action");
        $.ajax({
            type: method,
            data: $("#"+fromId).serialize(),
            url: url,
            dataType: 'json',
            success: function (res) {
                if (res.code == 1) {
                    $.confirm({
                        title: '',
                        content: res.msg,
                        type: 'blue',
                        buttons: {
                            omg: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function () {
                                    location.href = res.url;
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                } else {
                    $.alert({
                        title: '',
                        type: 'red',
                        content: res.msg
                    });
                }
            }, error: function (e) {
                $.alert({
                    title: '',
                    content: '请求异常，请重试'
                });
            }
        });
    });

    /**
     * 详情页的通用的保存按钮提交
     */
    $('#btn-save-detail').on('click', function () {
        var bt_obj = $('#btn-save-detail');
        var fromId = bt_obj.parents('form').attr('id');
        var method = bt_obj.parents('form').attr("method");
        var url = bt_obj.parents('form').attr("action");
        $.ajax({
            type: method,
            data: $("#"+fromId).serialize(),
            url: url,
            dataType: 'json',
            success: function (res) {
                if (res.code == 1) {
                    $.confirm({
                        title: '',
                        content: res.msg,
                        type: 'blue',
                        buttons: {
                            omg: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function () {
                                    location.href = res.url;
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                } else {
                    $.alert({
                        title: '',
                        type: 'red',
                        content: res.msg
                    });
                }
            }, error: function (e) {
                $.alert({
                    title: '',
                    content: '请求异常，请重试'
                });
            }
        });
    });
    
    /*
     通用更新提交
     */
    $('.btn-update').on('click', function () {
        var bt_obj = $('#button1');
        var fromId = bt_obj.parents('form').attr('id');
        var method = bt_obj.parents('form').attr("method");
        var url = bt_obj.parents('form').attr("action");
        $.ajax({
            type: method,
            data: $("#"+fromId).serialize(),
            url: url,
            dataType: 'json',
            success: function (res) {
                if (res.code == 1) {
                    $.confirm({
                        title: '',
                        content: res.msg,
                        type: 'blue',
                        buttons: {
                            omg: {
                                text: 'OK',
                                btnClass: 'btn-green',
                                action: function () {
                                    location.href = res.url;
                                }
                            },
                            close: function () {
                            }
                        }
                    });
                } else {
                    $.alert({
                        title: '错误',
                        type: 'red',
                        content: res.msg
                    });
                }
            }, error: function (e) {
                $.alert({
                    title: '异常',
                    content: '请求异常，请重试'
                });
            }
        });
    });
    
    $('.btn-search').on('click',function () {
        var bt_obj = $('#button1');
        var fromId = bt_obj.parents('form').attr('id');
        var method = bt_obj.parents('form').attr("method");
        var url = bt_obj.parents('form').attr("action");
        $("#list-ajax-custom-toolbar").data('url',url+'?'+$('#'+fromId).serialize());
    });

    //文本文件异步上传
    $('.btn-excel').change(function () {  //此处用了change事件，当选择好图片打开，关闭窗口时触发此事件
        $.ajaxFileUpload({
            url: '/admin/api/upload_uploadFile',   //处理图片的脚本路径
            type: 'post',       //提交的方式
            secureuri: false,   //是否启用安全提交
            fileElementId: 'UploadFile',     //file控件ID
            dataType: 'json',  //服务器返回的数据类型
            success: function (data) {  //提交成功后自动执行的处理函数
                if(data.code==1){
                    document.getElementById('xls').value = data.src;
                } else {
                    $.alert({
                        title: '',
                        type: 'red',
                        content: res.msg
                    });
                }
            },
            error: function (e) {   //提交失败自动执行的处理函数
                $.alert({
                    title: '',
                    content: '请求异常，请重试'
                });
            }
        })
    });

});
