/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : phper

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2017-07-30 14:05:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pe_category
-- ----------------------------
DROP TABLE IF EXISTS `pe_category`;
CREATE TABLE `pe_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID',
  `name` varchar(50) NOT NULL COMMENT '分类名称',
  `alias` varchar(50) DEFAULT '' COMMENT '导航别名',
  `content` longtext COMMENT '分类内容',
  `thumb` varchar(255) DEFAULT '' COMMENT '缩略图',
  `icon` varchar(20) DEFAULT '' COMMENT '分类图标',
  `template` varchar(50) NOT NULL DEFAULT '' COMMENT '分类模板',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '分类类型  1  列表  2 单页',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of pe_category
-- ----------------------------
INSERT INTO `pe_category` VALUES ('2', '0', '其他交流', '', '', '', '', '', '1', '2', null, '2016-12-04 19:49:30');
INSERT INTO `pe_category` VALUES ('4', '1', 'PHP技术', '', '', '', '', '', '1', '0', '2016-12-04 19:46:24', '2016-12-04 19:46:24');
INSERT INTO `pe_category` VALUES ('5', '1', '框架', '', '', '', '', '', '1', '0', '2016-12-04 19:46:34', '2016-12-04 19:46:34');
INSERT INTO `pe_category` VALUES ('1', '0', 'PHP相关', 'PHP相关', '', '', '', '', '1', '0', '2016-12-04 19:45:04', '2016-12-04 19:49:43');
INSERT INTO `pe_category` VALUES ('6', '1', '缓存', '', '', '', '', '', '1', '0', '2016-12-04 19:46:39', '2016-12-04 19:46:39');
INSERT INTO `pe_category` VALUES ('7', '1', 'MYSQL', '', '', '', '', '', '1', '0', '2016-12-04 19:46:43', '2016-12-04 19:46:43');
INSERT INTO `pe_category` VALUES ('8', '1', '函数库', '', '', '', '', '', '1', '0', '2016-12-04 19:46:48', '2016-12-04 19:46:48');
INSERT INTO `pe_category` VALUES ('9', '2', '技术闲谈', '', '', '', '', '', '1', '0', '2016-12-04 19:46:57', '2016-12-04 19:46:57');
INSERT INTO `pe_category` VALUES ('10', '2', '建议反馈', '', '', '', '', '', '1', '0', '2016-12-04 19:47:01', '2016-12-04 19:47:01');
INSERT INTO `pe_category` VALUES ('11', '2', '官方公告', '', '', '', '', '', '1', '0', '2016-12-04 19:47:05', '2016-12-04 19:47:05');

-- ----------------------------
-- Table structure for pe_collection
-- ----------------------------
DROP TABLE IF EXISTS `pe_collection`;
CREATE TABLE `pe_collection` (
  `uid` int(10) DEFAULT NULL,
  `tid` int(10) DEFAULT NULL,
  `add_time` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pe_collection
-- ----------------------------
INSERT INTO `pe_collection` VALUES ('3', '5', '2017-07-27 11:08:36');
INSERT INTO `pe_collection` VALUES ('3', '6', '2017-07-27 14:34:45');

-- ----------------------------
-- Table structure for pe_experience_log
-- ----------------------------
DROP TABLE IF EXISTS `pe_experience_log`;
CREATE TABLE `pe_experience_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `type` char(4) COLLATE utf8_bin NOT NULL COMMENT '银两状态 （+5，-5）',
  `motion` tinyint(10) NOT NULL DEFAULT '1' COMMENT '1 注册会员 2 发帖 3 精华 4采纳',
  `forum_uid` int(11) DEFAULT NULL COMMENT '来之',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='银两交易日志表';

-- ----------------------------
-- Records of pe_experience_log
-- ----------------------------

-- ----------------------------
-- Table structure for pe_forum
-- ----------------------------
DROP TABLE IF EXISTS `pe_forum`;
CREATE TABLE `pe_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT 'UID',
  `title` varchar(100) DEFAULT '' COMMENT '//文字标题',
  `top` tinyint(1) DEFAULT '0' COMMENT '1置顶',
  `elite` tinyint(1) DEFAULT '0' COMMENT '1精贴',
  `accept` tinyint(1) DEFAULT '0' COMMENT '0未结贴  1已结贴（采纳）',
  `color` varchar(10) DEFAULT NULL COMMENT '颜色',
  `tag` varchar(100) DEFAULT '' COMMENT '//关键词',
  `content` longtext COMMENT '//文章内容',
  `add_time` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT '发布时间',
  `view` int(11) DEFAULT '0' COMMENT '//查看次数',
  `cate_id` int(10) DEFAULT '0' COMMENT '//分类ID',
  `experience` int(10) DEFAULT '0' COMMENT '悬赏银两',
  `status` tinyint(1) DEFAULT '1' COMMENT '1正常  -1删除',
  `create_time` datetime DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`,`uid`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='//文章表';

-- ----------------------------
-- Records of pe_forum
-- ----------------------------

-- ----------------------------
-- Table structure for pe_forum_reply
-- ----------------------------
DROP TABLE IF EXISTS `pe_forum_reply`;
CREATE TABLE `pe_forum_reply` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tid` int(10) DEFAULT NULL COMMENT '帖子ID',
  `uid` int(10) DEFAULT NULL COMMENT '回复UID',
  `touid` int(10) DEFAULT '0' COMMENT '回复谁谁谁的UID',
  `floorid` char(20) COLLATE utf8_bin DEFAULT NULL COMMENT '楼层ID',
  `content` longtext COLLATE utf8_bin,
  `best` tinyint(1) DEFAULT '0' COMMENT '最佳答案 1 （采纳）',
  `zan` int(10) DEFAULT '0' COMMENT '点赞',
  `isread` tinyint(1) DEFAULT '0' COMMENT '是否已阅',
  `isdel` tinyint(1) DEFAULT '0' COMMENT '是否删除 0正常 1删除',
  `noticeclear` tinyint(1) DEFAULT '1' COMMENT '是否清空消息通知 1正常 -1删除',
  `reply_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='内容回复表';

-- ----------------------------
-- Records of pe_forum_reply
-- ----------------------------

-- ----------------------------
-- Table structure for pe_guest
-- ----------------------------
DROP TABLE IF EXISTS `pe_guest`;
CREATE TABLE `pe_guest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL COMMENT '访问对象uid',
  `fuid` int(10) NOT NULL COMMENT '访问来源uid',
  `guest_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of pe_guest
-- ----------------------------

-- ----------------------------
-- Table structure for pe_level
-- ----------------------------
DROP TABLE IF EXISTS `pe_level`;
CREATE TABLE `pe_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(40) COLLATE utf8_bin DEFAULT NULL COMMENT '组名称',
  `remark` char(40) COLLATE utf8_bin DEFAULT NULL COMMENT '组备注',
  `status` tinyint(1) DEFAULT '1' COMMENT '1正常 -1不可用',
  `sort` tinyint(2) DEFAULT '0' COMMENT '排序',
  `gid` tinyint(1) DEFAULT '0' COMMENT '0普通会员 1 管理员  2超级管理员',
  `cretae_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='用户组表';

-- ----------------------------
-- Records of pe_level
-- ----------------------------
INSERT INTO `pe_level` VALUES ('-1', '游客', '邮箱未激活用户组', '1', '0', '0', '2017-07-28 15:13:37');
INSERT INTO `pe_level` VALUES ('1', '会员', '注册会员用户组', '1', '1', '0', '2017-07-28 15:13:37');
INSERT INTO `pe_level` VALUES ('2', 'VIP', '赞赏超过20', '1', '2', '0', '2017-07-28 15:13:37');
INSERT INTO `pe_level` VALUES ('5', 'VIP1', '赞赏超过100', '1', '5', '0', '2017-07-28 15:13:37');
INSERT INTO `pe_level` VALUES ('6', 'VIP2', '赞赏超过200', '1', '7', '0', '2017-07-28 15:13:37');
INSERT INTO `pe_level` VALUES ('7', 'VIP3', '赞赏超过300', '1', '9', '0', '2017-07-28 15:13:37');
INSERT INTO `pe_level` VALUES ('9', 'SVIP', '赞赏超过500', '1', '66', '0', '2017-07-28 15:13:37');
INSERT INTO `pe_level` VALUES ('10', '鬼王', null, '1', '77', '0', '2017-07-28 15:13:37');
INSERT INTO `pe_level` VALUES ('98', '管理员', '管理员', '1', '88', '1', '2017-07-28 15:13:37');
INSERT INTO `pe_level` VALUES ('99', '站长', '站点超管', '1', '99', '2', '2017-07-28 15:13:37');

-- ----------------------------
-- Table structure for pe_links
-- ----------------------------
DROP TABLE IF EXISTS `pe_links`;
CREATE TABLE `pe_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '//名称',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '//简介',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '//URL',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '//排序',
  PRIMARY KEY (`link_id`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='//友情链接表';

-- ----------------------------
-- Records of pe_links
-- ----------------------------
INSERT INTO `pe_links` VALUES ('10', 'PHPer社区', 'PHPer社区', 'http://zz1.com.cn', '0');

-- ----------------------------
-- Table structure for pe_nav
-- ----------------------------
DROP TABLE IF EXISTS `pe_nav`;
CREATE TABLE `pe_nav` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL COMMENT '父ID',
  `name` varchar(20) NOT NULL COMMENT '导航名称',
  `alias` varchar(20) DEFAULT '' COMMENT '导航别称',
  `link` varchar(255) DEFAULT '' COMMENT '导航链接',
  `icon` varchar(255) DEFAULT '' COMMENT '导航图标',
  `target` varchar(10) DEFAULT '' COMMENT '打开方式',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态  0 隐藏  1 显示',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='导航表';

-- ----------------------------
-- Records of pe_nav
-- ----------------------------
INSERT INTO `pe_nav` VALUES ('1', '0', '问答', '问答', '/forum', 'iconfont icon-wenda', '_self', '1', '0', '2016-12-04 16:24:35', '2016-12-04 16:48:31');
INSERT INTO `pe_nav` VALUES ('2', '0', '阳阳的博客', '阳阳的博客', 'http://zz1.com.cn', 'iconfont icon-iconmingxinganli', '_blank', '1', '1', '2016-12-04 16:26:00', '2016-12-04 16:26:00');

-- ----------------------------
-- Table structure for pe_record_log
-- ----------------------------
DROP TABLE IF EXISTS `pe_record_log`;
CREATE TABLE `pe_record_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(1) DEFAULT NULL COMMENT '来源',
  `uid` int(10) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `module` varchar(255) DEFAULT NULL,
  `controller` varchar(255) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `record_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pe_record_log
-- ----------------------------

-- ----------------------------
-- Table structure for pe_system
-- ----------------------------
DROP TABLE IF EXISTS `pe_system`;
CREATE TABLE `pe_system` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '配置项名称',
  `value` text NOT NULL COMMENT '配置项值',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- ----------------------------
-- Records of pe_system
-- ----------------------------
INSERT INTO `pe_system` VALUES ('1', 'site_config', 'a:8:{s:10:\"site_title\";s:11:\"PHPer社区\";s:9:\"seo_title\";s:18:\"免费开源社区\";s:11:\"seo_keyword\";s:63:\"基于ThinkPHP5最新版本开发的Fly社区模版开源下载\";s:15:\"seo_description\";s:0:\"\";s:8:\"site_url\";s:17:\"http://zz1.com.cn\";s:14:\"site_copyright\";s:9:\"ysongyang\";s:8:\"site_icp\";s:22:\"豫ICP备14022012号-2\";s:11:\"site_tongji\";s:279:\"<script>\r\nvar _hmt = _hmt || [];\r\n(function() {\r\n  var hm = document.createElement(\"script\");\r\n  hm.src = \"https://hm.baidu.com/hm.js?9f47e0a59173fe5ecaf8340f5283b3ef\";\r\n  var s = document.getElementsByTagName(\"script\")[0]; \r\n  s.parentNode.insertBefore(hm, s);\r\n})();\r\n</script>\";}');

-- ----------------------------
-- Table structure for pe_user
-- ----------------------------
DROP TABLE IF EXISTS `pe_user`;
CREATE TABLE `pe_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` char(32) DEFAULT NULL COMMENT '记录openid',
  `sina_openid` char(32) DEFAULT NULL COMMENT '新浪openid',
  `sina_token` char(40) DEFAULT NULL COMMENT '新浪token',
  `email` varchar(40) DEFAULT NULL COMMENT '邮箱',
  `username` varchar(50) DEFAULT NULL COMMENT '昵称',
  `password` varchar(255) DEFAULT NULL COMMENT '密码 ，PC端需要',
  `headimgurl` varchar(200) DEFAULT NULL COMMENT '头像',
  `age` int(3) DEFAULT '0' COMMENT '年龄',
  `mobile` varchar(11) DEFAULT NULL COMMENT '手机号',
  `gender` tinyint(1) DEFAULT '0' COMMENT '1男 2女',
  `country` char(30) DEFAULT '中国' COMMENT '国家',
  `province` char(30) DEFAULT NULL COMMENT '省份',
  `city` char(30) DEFAULT NULL COMMENT '市区',
  `area` char(30) DEFAULT NULL COMMENT '区域',
  `address` varchar(200) DEFAULT NULL COMMENT '详细地址',
  `sign` varchar(100) DEFAULT '' COMMENT '个人签名',
  `experience` int(10) DEFAULT '0' COMMENT '积分',
  `qq` int(10) DEFAULT NULL COMMENT 'qq',
  `loginnum` int(10) DEFAULT '0' COMMENT '登录次数',
  `regdate` datetime DEFAULT NULL COMMENT '注册时间',
  `regip` char(15) DEFAULT NULL COMMENT '注册IP',
  `lastip` char(15) DEFAULT NULL COMMENT '登录IP',
  `lastdate` timestamp NULL DEFAULT NULL COMMENT '最后一次登录时间',
  `level` tinyint(1) DEFAULT '-1' COMMENT '-1 游客 1 会员 2 VIP  3 SVIP 99 Super Admin',
  `identifier` varchar(32) DEFAULT NULL COMMENT '第二身份标识',
  `token` varchar(32) DEFAULT NULL COMMENT '永久登录标识',
  `timeout` int(11) DEFAULT NULL COMMENT '永久登录超时时间',
  `activated` smallint(1) DEFAULT '0' COMMENT '0未激活，1已激活',
  `lock` smallint(1) DEFAULT '1' COMMENT '用户状态  1 正常 0 禁止',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='//会员表';

-- ----------------------------
-- Records of pe_user
-- ----------------------------

-- ----------------------------
-- Table structure for pe_verify
-- ----------------------------
DROP TABLE IF EXISTS `pe_verify`;
CREATE TABLE `pe_verify` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question` char(100) COLLATE utf8_bin DEFAULT NULL COMMENT '问题',
  `answer` char(50) COLLATE utf8_bin DEFAULT NULL COMMENT '答案',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of pe_verify
-- ----------------------------
INSERT INTO `pe_verify` VALUES ('1', '1+1=？', '2');
INSERT INTO `pe_verify` VALUES ('2', '爱PHPer社区吗？请回答：爱', '爱');
INSERT INTO `pe_verify` VALUES ('3', 'a和c之间的字母是？', 'b');
INSERT INTO `pe_verify` VALUES ('4', 'PHPer社区的网址是多少？（不带http://）', 'zz1.com.cn');
INSERT INTO `pe_verify` VALUES ('5', '\"1 3 2 4 6 5 7 ___\" 请写出\"____\"处的数字', '9');
INSERT INTO `pe_verify` VALUES ('6', '44加36等于几？', '80');
INSERT INTO `pe_verify` VALUES ('7', '1+2+3+4+5+6+8+9等于几？', '38');
INSERT INTO `pe_verify` VALUES ('8', '请在输入框输入“撸代码”', '撸代码');
INSERT INTO `pe_verify` VALUES ('9', '社区采用TP5编写，yes or no？', 'yes');
INSERT INTO `pe_verify` VALUES ('10', '\"100\" > \"2\" 的结果是true 还是 false？', 'false');

-- ----------------------------
-- Table structure for pe_zan
-- ----------------------------
DROP TABLE IF EXISTS `pe_zan`;
CREATE TABLE `pe_zan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(10) DEFAULT NULL COMMENT '回复内容id',
  `uid` int(10) DEFAULT NULL,
  `ip` char(40) DEFAULT NULL COMMENT 'IP',
  `isdel` tinyint(1) DEFAULT '0' COMMENT '状态 1删除 0正常',
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pe_zan
-- ----------------------------
