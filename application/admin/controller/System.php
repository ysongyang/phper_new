<?php

namespace app\admin\controller;

use think\Cache;
use think\Db;
use think\Session;

/**
 * 系统配置
 * Class System
 * @package app\admin\controller
 */
class System extends Base
{
    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 站点配置
     */
    public function index()
    {
        if (Cache::has('site_config')) {
            $site_config = Cache::get('site_config');
        } else {
            $site_config = Db::name('system')->field('value')->where('name', 'site_config')->find();
            $site_config = unserialize($site_config['value']);
        }
        return $this->fetch('index', ['site_config' => $site_config]);
    }

    /**
     * 提交配置
     */
    public function update()
    {
        if ($this->request->isPost()) {
            $site_config = $this->request->post('site_config/a');
            $data['value'] = serialize($site_config);
            if (Db::name('system')->where('name', 'site_config')->update($data) !== false) {
                Cache::set('site_config', $site_config);
                $this->insertLog(Session::get('admin_id'), '更新配置信息');
                $this->success('提交成功');
            } else {
                $this->error('提交失败');
            }
        }
    }

    /**
     * 清除缓存
     */
    public function clear()
    {
        if (delete_dir_file(CACHE_PATH) || delete_dir_file(TEMP_PATH) || delete_dir_file(LOG_PATH)) {
            $data['code'] = 0;
            $data['msg'] = '清除缓存成功';
            return json($data);
        } else {
            $this->error('清除缓存失败');
        }
    }
}