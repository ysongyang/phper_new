<?php
/**
 * @Notes by 注册用户控制器.
 * @author: ysongyang <ysongyang@qq.com>
 * @link
 * @copyright: copyright 2017 ysongyang all rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace app\admin\controller;

use app\model\Level;
use app\model\User as UserModel;
use think\Session;

class User extends Base
{

    protected function _initialize()
    {
        parent::_initialize();

    }

    public function index()
    {

        return $this->fetch();
    }

    public function getUserJson()
    {
        $user_model = new UserModel();
        $list = $user_model->getUserList('');
        unset($user_model);
        echo json_encode($list, true);
    }

    /**
     * 添加
     */
    public function add()
    {
        $args = $this->request->param();
        $user_model = new UserModel();
        $user_data = $user_model->field('id,name,mobile')->select();
        unset($user_model);
        return $this->fetch('add', ['user_data' => $user_data, 'args' => $args]);
    }

    /**
     * 保存
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (empty($data['name'])) {
                $this->error('姓名不能为空');
            }
            if (empty($data['birthday'])) {
                $this->error('请输入出生日期');
            }
            if (empty($data['mobile'])) {
                $this->error('请输入手机号');
            }
            if (empty($data['province_code']) || empty($data['city_code'])) {
                $this->error('地址不能为空');
            }
        }
    }

    /**
     * 编辑
     */
    public function edit($id)
    {
        $user_model = new UserModel();
        $user_data = $user_model->getUserList($id);
        $level_model = new Level();
        $level_data = $level_model->getListAll();
        unset($user_model);
        unset($level_model);
        return $this->fetch('edit', ['user_data' => $user_data[0], 'level_data' => $level_data]);
    }

    /**
     * 更新
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $user_model = new UserModel();
            $check_um = $user_model->search(['username' => ['like', "%{$data['username']}%"], 'id' => array('neq', $id)], 'username');
            if (is_numeric($data['username'])) {
                $this->error('昵称不能全为数字');
            } elseif ($check_um) {
                $this->error('该昵称已存在');
            }
            if (!empty($data['password'])) {
                if (strlen($data['password']) < 6 || strlen($data['password']) > 16) {
                    $this->error('密码必须为6到16个字符');
                }
                //$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
				$data['password'] = authcode($data['password'], 'ENCODE', '', 0);
            } else {
                unset($data['password']);
            }
            if (!empty($data['mobile'])) {
                if (!isMobile($data['mobile'])) $this->error('手机号格式有误');
            }
            $data['lock'] = empty($data['lock']) == true ? 0 : 1;
            if ($user_model->allowField(true)->save($data, ['id' => $id]) !== false) {
                $this->insertLog(Session::get('admin_id'), '更新账号(' . $data['email'] . ')，ID为' . $id);
                $this->success('更新成功');
            } else {
                $this->error('更新失败');
            }
        }
    }

    /**
     * 删除
     */
    public function delete($id)
    {

    }

}