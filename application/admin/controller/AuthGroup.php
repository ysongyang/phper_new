<?php

namespace app\admin\controller;

use app\model\AuthGroupAccess;
use app\model\AuthRule;
use app\model\AuthGroup as AuthGroupModel;
use think\Session;

/**
 * 角色管理
 * Class AdminUser
 * @package app\admin\controller
 */
class AuthGroup extends Base
{

    protected function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 角色管理
     * @param $page
     * @return mixed
     */
    public function index($page = 1)
    {
        $auth_role_model = new AuthGroupModel();
        $auth_role_list = $auth_role_model->order('id DESC')->paginate(20, false, ['page' => $page]);
        return $this->fetch('index', ['auth_role_list' => $auth_role_list]);
    }

    /**
     * 添加角色
     * @return mixed
     */
    public function add()
    {
        return $this->fetch();
    }

    /**
     * 保存角色
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (empty($data['title'])) {
                $this->error('角色名称不能为空');
            }
            $data['create_time'] = date('Y-m-d H:i:s');
            $auth_role_model = new AuthGroupModel();
            if ($auth_role_model->allowField(true)->save($data) !== false) {
                $this->insertLog(Session::get('admin_id'), '添加用户组（' . $data['title'] . '）,ID为' . $auth_role_model->getLastInsID());
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        }
    }

    /**
     * 编辑角色
     * 规则：
     * 如果编辑的用户是超级角色，自己不是超级角色用户组，则无法进行编辑  反之 可以
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $auth_role_model = new AuthGroupModel();
        $map['id'] = $id;
        $info = $auth_role_model->where($map)->find();
        return $this->fetch('edit', ['info' => $info]);
    }

    /**
     * 更新角色
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if ($id == 1 && $data['status'] != 1) {
                $this->error('超级管理组不可禁用');
            }
            $auth_role_model = new AuthGroupModel();
            if ($auth_role_model->save($data, $id) !== false) {
                $this->insertLog(Session::get('admin_id'), '更新用户组（' . $data['title'] . '）,ID为' . $id);
                $this->success('更新成功');
            } else {
                $this->error('更新失败');
            }
        }
    }

    /**
     * 删除角色
     * @param $id
     */
    public function delete($id)
    {
        if ($id <= 3) {
            $this->error('系统默认用户组不可删除');
        }
        $auth_role_model = new AuthGroupModel();
        $data = $auth_role_model->field('title')->find($id);
        if ($auth_role_model->destroy($id)) {
            $auth_group_access = new AuthGroupAccess();
            //删除用户与组的关系表
            $auth_group_access->where(['group_id' => $id])->delete();
            $this->insertLog(Session::get('admin_id'), '删除用户组（' . $data['title'] . '）,ID为' . $id);
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }


    /**
     * 授权
     * @param $id
     * @return mixed
     */
    public function auth($id)
    {
        $auth_role_model = new AuthGroupModel();
        $auth_role = $auth_role_model->field('title')->find($id);
        return $this->fetch('auth', ['id' => $id, 'auth' => $auth_role]);
    }

    /**
     * AJAX获取规则数据
     * @param $id
     * @return mixed
     */
    public function getJson($id)
    {
        $auth_role_model = new AuthGroupModel();
        $auth_rule_model = new AuthRule();
        $auth_group_data = $auth_role_model->find($id);
        $auth_rules = explode(',', $auth_group_data['rules']);
        $auth_rule_list = $auth_rule_model->field('id,pid,title')->order(['sort' => 'ASC', 'id' => 'ASC'])->select();
        foreach ($auth_rule_list as $key => $value) {
            in_array($value['id'], $auth_rules) && $auth_rule_list[$key]['checked'] = true;
            //$auth_rule_list[$key]['open'] = true; //默认展开所有节点
        }
        echo json_encode($auth_rule_list, true);
    }

    /**
     * 更新权限组规则
     * @param $id
     * @param $auth_rule_ids
     */
    public function updateAuthGroupRule($id, $auth_rule_ids = '')
    {
        $auth_role_model = new AuthGroupModel();
        if ($this->request->isPost()) {
            if ($id) {
                $group_data['id'] = $id;
                $group_data['rules'] = is_array($auth_rule_ids) ? implode(',', $auth_rule_ids) : '';
                $group = $auth_role_model->field("title")->find($id);
                if ($auth_role_model->save($group_data, $id) !== false) {
                    $this->insertLog(Session::get('admin_id'), '对用户组（' . $group['title'] . '）进行授权');
                    $this->success('授权成功');
                } else {
                    $this->error('授权失败');
                }
            }
        }
    }
}