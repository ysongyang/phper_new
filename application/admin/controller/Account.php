<?php

namespace app\admin\controller;

use app\model\Admin;
use app\model\AuthGroup;
use app\model\AuthGroupAccess;
use think\Session;

/**
 * 管理员管理
 * Class AdminUser
 * @package app\admin\controller
 */
class Account extends Base
{

    protected function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 管理员管理
     * @return mixed
     */
    public function index()
    {
        return $this->fetch();
    }

    public function getAccountJson()
    {
        $user_model = new Admin();
        $list = $user_model->getAdminUserList(null);
        foreach ($list as $key => $val) {
            //$list[$key]['dist_code'] = getCityRetrunCode($val['province_code'], $val['city_code'], $val['area_code']);
            $list[$key]['status'] = $val['status'] == 1 ? "正常" : "<span style='color: red'>锁定</span>";
        }
        echo json_encode($list, true);
    }

    /**
     * 添加管理员
     * @return mixed
     */
    public function add()
    {
        $auth_role = new AuthGroup();
        $auth_role_list = $auth_role->field('id,title')->select();
        return $this->fetch('add', ['auth_role_list' => $auth_role_list]);
    }

    /**
     * 保存管理员
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (empty($data['username'])) {
                $this->error('用户名不能为空');
            }
            if (empty($data['password'])) {
                $this->error('密码不能为空');
            }
            if ($data['password'] !== $data['confirm_password']) {
                $this->error('两次密码输入不一致');
            }
            if (!empty($data['mobile'])) {
                if (!isMobile($data['mobile'])) {
                    $this->error('手机号格式有误');
                }
            }
            $user_model = new Admin();
            $where['username'] = $data['username'];
            $where1['mobile'] = $data['mobile'];
            if ($user_model->search($where, $where1)) {
                $this->error('用户名或手机号已经存在，请重新输入');
            }
            $data['password'] = authcode($data['password'], 'ENCODE', '', 0);
            $data['reg_time'] = date('Y-m-d H:i:s');
            $data['reg_ip'] = get_client_ip();
            if ($user_model->allowField(true)->save($data) !== false) {
                $this->insertLog(Session::get('admin_id'), '添加账号（' . $data['username'] . '），ID为' . $user_model->getLastInsID());
                $this->success('添加成功!');
            } else {
                $this->error('添加失败');
            }
        }
    }

    /**
     * 编辑管理员
     * 规则：
     * 如果编辑的用户是超级管理员，自己不是超级管理员用户组，则无法进行编辑  反之 可以
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $auth_user_role = new AuthGroupAccess();
        if ($auth_user_role->checkAuth(Session::get('admin_id'), $id) == true) {
            $this->error('您不能操作自己的账号或比自己权限大的用户账号');
        }
        $user_model = new Admin();
        $info = $user_model->getAdminUserList($id);
        $auth_role_model = new AuthGroup();
        $role_list = $auth_role_model->getRoleList(Session::get('admin_id'));
        return $this->fetch('edit', ['info' => $info[0], 'role_list' => $role_list]);
    }

    /**
     * 更新管理员
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            if (empty($data['username'])) {
                $this->error('用户名不能为空');
            }
            if (!empty($data['password'])) {
                if ($data['password'] !== $data['confirm_password']) {
                    $this->error('两次密码输入不一致');
                }
                $data['password'] = authcode($data['password'], 'ENCODE', '', 0);
            } else {
                unset($data['password']);
            }
            if (!empty($data['mobile'])) {
                if (!isMobile($data['mobile'])) {
                    $this->error('手机号格式有误');
                }
            }
            $user_model = new Admin();
            $user_model->allowField(true)->save($data, ['id' => $id]);
            $auth_user_role_model = new AuthGroupAccess();
            $old_map['uid'] = $id;
            $old_map['group_id'] = !empty($data['old_group_id']) ? $data['old_group_id'] : 0;
            $a_u_r_data = $auth_user_role_model->where($old_map)->find();
            if (!empty($a_u_r_data) && $a_u_r_data['group_id'] == $data['group_id']) {
                $this->success('更新成功');
            } else {
                $auth_user_role_model->where($old_map)->delete();
                if (!empty($data['group_id'])) {
                    $data_arr['uid'] = $id;
                    $data_arr['group_id'] = $data['group_id'];
                    $data_arr['create_time'] = date('Y-m-d H:i:s');
                    $auth_user_role_model->save($data_arr);
                }
                $this->insertLog(Session::get('admin_id'), '更新账号（' . $data['username'] . '），ID为' . $id);
                $this->success('更新成功');
            }
            $this->error('更新失败');
        }
    }

    /**
     * 删除管理员
     * @param $id
     */
    public function delete($id)
    {
        if ($id == 1) {
            $this->error('默认管理员不可删除');
        }
        $user_model = new Admin();
        $userName = $user_model->getUserField($id, 'username');
        if ($user_model->destroy($id)) {
            $this->insertLog(Session::get('admin_id'), '删除账号（' . $userName . '）');
            $auth_user_role = new AuthGroupAccess();
            $auth_user_role->where('uid', $id)->delete();
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

}