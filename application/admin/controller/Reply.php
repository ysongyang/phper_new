<?php

namespace app\admin\controller;

use app\model\Forum;
use app\model\ForumReply;
use think\Session;

/**
 * 回答管理
 * Class Article
 * @package app\admin\controller
 */
class Reply extends Base
{

    protected function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 获取回复列表
     * @return mixed
     */
    public function index()
    {
        return $this->fetch();
    }

    public function getReplyJson()
    {
        $reply_model = new ForumReply();
        $list = $reply_model->getListAll();
        echo json_encode($list, true);
    }

    public function view($id)
    {
        $reply_model = new ForumReply();
        $floorid = $reply_model->getField($id, 'floorid');
        $tid = $reply_model->getField($id, 'tid');
        header('Location:' . DO_MAIN . url('index/forum/detail', ['tid' => $tid]) . $floorid);
    }

    /**
     * 删除文章
     * @param int $id
     */
    public function delete($id)
    {
        $reply_model = new ForumReply();
        if ($id) {
            if ($reply_model->destroy($id)) {
                $this->insertLog(Session::get('admin_id'), '删除回复，ID为' . $id);
                $this->success('删除成功');
            } else {
                $this->error('删除失败');
            }
        } else {
            $this->error('请选择需要删除的回答');
        }
    }
}