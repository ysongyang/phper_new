<?php

namespace app\admin\controller;

use app\model\Category as CategoryModel;
use think\Session;

/**
 * 栏目管理
 * Class Category
 * @package app\admin\controller
 */
class Category extends Base
{

    protected function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 栏目管理
     * @return mixed
     */
    public function index()
    {
        $category_model = new CategoryModel();
        $category_level_list = $category_model->getLevelList();
        unset($category_model);
        $this->assign('category_level_list', $category_level_list);
        return $this->fetch();
    }

    /**
     * 添加栏目
     * @param string $pid
     * @return mixed
     */
    public function add($pid = '')
    {
        $category_model = new CategoryModel();
        $category_level_list = $category_model->getLevelList();
        unset($category_model);
        $this->assign('category_level_list', $category_level_list);
        return $this->fetch('add', ['pid' => $pid]);
    }

    /**
     * 保存栏目
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $validate_result = $this->validate($data, 'Category');
            $category_model = new CategoryModel();
            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if ($category_model->allowField(true)->save($data)) {
                    $this->insertLog(Session::get('admin_id'), '添加栏目（' . $data['name'] . '），ID为' . $category_model->id);
                    unset($category_model);
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑栏目
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $category_model = new CategoryModel();
        $category = $category_model->find($id);
        $category_level_list = $category_model->getLevelList();
        unset($category_model);
        $this->assign('category_level_list', $category_level_list);
        return $this->fetch('edit', ['category' => $category]);
    }

    /**
     * 更新栏目
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $validate_result = $this->validate($data, 'Category');
            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                $category_model = new CategoryModel();
                if ($category_model->allowField(true)->save($data, $id) !== false) {
                    unset($category_model);
                    $this->insertLog(Session::get('admin_id'), '更新栏目（' . $data['name'] . '），ID为' . $id);
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');
                }
            }
        }
    }

    /**
     * 删除栏目
     * @param $id
     */
    public function delete($id)
    {
        $category = CategoryModel::get(['pid' => $id]);
        $category_model = new CategoryModel();
        if (!empty($category)) {
            $this->error('此分类下存在子分类，不可删除');
        }
        $cateName = $category_model->getField($id, 'name');
        if ($category_model->destroy($id)) {
            unset($category_model);
            $this->insertLog(Session::get('admin_id'), '删除栏目（' . $cateName . ')，ID为' . $id);
            $this->success('删除成功');
        } else {
            $this->error('删除失败');
        }
    }

    //异步排序
    public function change()
    {
        $id = $this->request->param('id');
        $sort = $this->request->param('sort');
        if (!is_numeric($sort)) {
            $this->error('排序只能是数字');
        }
        //更新当前栏目的排序
        $category_model = new CategoryModel();
        $data['sort'] = $sort;
        if ($category_model->save($data, ['id' => $id]) !== false) {
            unset($category_model);
            $this->success('栏目排序更新成功', url('admin/Category/index'));
        } else {
            $this->error('栏目排序更新失败，请稍后再试！');
        }
    }
}