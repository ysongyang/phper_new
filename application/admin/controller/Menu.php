<?php

namespace app\admin\controller;

use app\model\AuthRule;
use think\Session;

/**
 * 后台菜单
 * Class Menu
 * @package app\admin\controller
 */
class Menu extends Base
{

    protected function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 后台菜单
     * @return mixed
     */
    public function index()
    {
        $auth_rule_model = new AuthRule();
        $admin_menu_list = $auth_rule_model->order(['sort' => 'ASC', 'id' => 'ASC'])->select();
        $admin_menu_level_list = array2level($admin_menu_list);
        $this->assign('admin_menu_level_list', $admin_menu_level_list);

        return $this->fetch();
    }

    /**
     * 添加菜单
     * @param string $pid
     * @return mixed
     */
    public function add($pid = '')
    {
        $auth_rule_model = new AuthRule();
        $admin_menu_list = $auth_rule_model->order(['sort' => 'ASC', 'id' => 'ASC'])->select();
        $admin_menu_level_list = array2level($admin_menu_list);
        $this->assign('admin_menu_level_list', $admin_menu_level_list);
        return $this->fetch('add', ['pid' => $pid]);
    }

    /**
     * 保存菜单
     */
    public function save()
    {
        if ($this->request->isPost()) {
            $auth_rule_model = new AuthRule();
            $data = $this->request->post();
            $validate_result = $this->validate($data, 'Menu');
            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if ($auth_rule_model->save($data)) {
                    $this->insertLog(Session::get('admin_id'), '添加菜单（' . $data['title'] . '），ID为' . $auth_rule_model->id);
                    $this->success('保存成功');
                } else {
                    $this->error('保存失败');
                }
            }
        }
    }

    /**
     * 编辑菜单
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $auth_rule_model = new AuthRule();
        $admin_menu = $auth_rule_model->find($id);
        $admin_menu_list = $auth_rule_model->order(['sort' => 'ASC', 'id' => 'ASC'])->select();
        $admin_menu_level_list = array2level($admin_menu_list);
        $this->assign('admin_menu_level_list', $admin_menu_level_list);
        return $this->fetch('edit', ['admin_menu' => $admin_menu]);
    }

    /**
     * 更新菜单
     * @param $id
     */
    public function update($id)
    {
        if ($this->request->isPost()) {
            $auth_rule_model = new AuthRule();
            $data = $this->request->post();
            $validate_result = $this->validate($data, 'Menu');
            if ($validate_result !== true) {
                $this->error($validate_result);
            } else {
                if ($auth_rule_model->save($data, $id) !== false) {
                    $this->insertLog(Session::get('admin_id'), '更新菜单（' . $data['title'] . '），ID为' . $id);
                    $this->success('更新成功');
                } else {
                    $this->error('更新失败');

                }
            }
        }
    }

    /**
     * 删除菜单
     * @param $id
     */
    public function delete($id)
    {
        $auth_rule_model = new AuthRule();
        $delete_auth_rule = $auth_rule_model->selectAll($id);
        $menuName = $auth_rule_model->field('title')->where(['id' => $id])->find();
        if ($delete_auth_rule) {
            $this->error('请先删除子级菜单');
        } elseif ($auth_rule_model->destroy($id)) {
            $this->insertLog(Session::get('admin_id'), '删除菜单（' . $menuName['title'] . '），ID为' . $id);
            $this->success('删除成功', url('admin/Menu/index'));
        } else {
            $this->error('删除失败');
        }
    }


    //异步排序
    public function change()
    {
        $id = $this->request->param('id');
        $sort = $this->request->param('sort');
        if (!is_numeric($sort)) {
            $this->error('排序只能是数字');
        }
        //更新当前栏目的排序
        $auth_rule_model = new AuthRule();
        $data['sort'] = $sort;
        $res = $auth_rule_model->save($data, ['id' => $id]);
        if ($res) {
            $this->success('栏目排序更新成功', url('admin/Menu/index'));
        } else {
            $this->error('栏目排序更新失败，请稍后再试！');
        }
    }

}