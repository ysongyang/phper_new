<?php
/**
 * @Notes 积分操作日子表
 * 1、注册会员 记录         默认赠送50银两
 * 2、发布问答 需要记录     记录扣除设置的银两数量
 * 3、答案被采纳 需要记录     将发布问答人员的银两增加给被采纳人员
 * 4、设置精华   增加10银两
 * 字段：motion  动作（1 注册会员 2 发帖 3 精华 4采纳）
 * @author: ysongyang <ysongyang@qq.com>
 * @link
 * @copyright: copyright 2016 ysongyang all rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
namespace app\model;

use think\Model;
use think\Config;

class ExperienceLog extends Model
{
    protected $autoWriteTimestamp = 'datetime';
    protected $initial_integral;

    public function __construct()
    {
        $this->initial_integral=Config::get('default_integral');
        parent::__construct();
    }

    /**
     * 注册会员 记录  默认赠送50银两
     * @param $uid
     * @param int $motion
     * @return int|string
     */
    public function regUser($uid,$motion=1)
    {
        $data = array(
            'uid'=>$uid,
            'type'=>'+'.$this->initial_integral,
            'motion'=>$motion,
            'create_time'=>date('Y-m-d H:i:s')
        );
        $user_model = new User();
        $user_model->where(['id'=>$uid])->setInc('experience',$this->initial_integral); //更改会员的积分状态
        return $this->insert($data);
    }

    /**
     * 发布问答 需要记录     记录扣除设置的银两数量
     * @param $uid
     * @param $experience
     * @param int $motion
     */
    public function addForum($uid,$experience,$motion=2){
        $user_model = new User();
        $data = array(
            'uid'=>$uid,
            'type'=>'-'.$experience,
            'motion'=>$motion,
            'create_time'=>date('Y-m-d H:i:s')
        );
        $check=$user_model->where(['id'=>$uid])->setDec('experience',$experience); //更改会员的积分
        $chk=$this->insert($data);
        if($check && $chk){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 设置精华   增加银两
     * @param $uid
     * @param $tid
     */
    public function setElite($tid,$rank,$motion=3,$experience=10){
        $forum_model = new Forum();
        $user_model = new User();
        $forum = $forum_model->search(['id'=>$tid],"uid");
        if($rank){
            $data = array(
                'uid'=>$forum['uid'],
                'type'=>'+'.$experience,
                'motion'=>$motion,
                'create_time'=>date('Y-m-d H:i:s')
            );
            $user_model->where(['id'=>$forum['uid']])->setInc('experience',$experience); //更改会员的积分状态+
            return $this->insert($data);
        }else{
            $data = array(
                'uid'=>$forum['uid'],
                'type'=>'-'.$experience,
                'motion'=>$motion,
                'create_time'=>date('Y-m-d H:i:s')
            );
            $user_model->where(['id'=>$forum['uid']])->setDec('experience',$experience); //更改会员的积分状态-
            return $this->insert($data);
        }
    }

    /**
     * 答案被采纳 需要记录     将发布问答人员的银两增加给被采纳人员
     * @param $id  回复记录Id
     */
    public function setAccept($id,$motion=4){
        $forum_model = new Forum();
        $user_model = new User();
        $reply_D = db('reply')->find($id);
        $tid = $reply_D['tid']; //帖子ID
        $forum = $forum_model->search(['id'=>$tid],"uid,experience");
        $experience= $forum['experience']; //发帖人设置银两
        $fuid = $forum['uid']; //发帖人ID
        $uid = $reply_D['uid']; //回复人ID
        //操作给人员增加银两
        $user_model->where(['id'=>$uid])->setInc('experience',$experience); //被采纳 给回复人员增加悬赏
        //操作记录日志
        $data = array(
            'uid'=>$uid,
            'type'=>'+'.$experience,
            'motion'=>$motion,
            'forum_uid'=>$fuid,
            'create_time'=>date('Y-m-d H:i:s')
        );
        return $this->insert($data);
    }
}