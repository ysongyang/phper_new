<?php
/**
 * @Notes 回复信息模型.
 * @author: ysongyang <ysongyang@qq.com>
 * @link
 * @copyright: copyright 2016 ysongyang all rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace app\model;

use think\Model;

class ForumReply extends Model
{
    //获取所有回复
    public function getListAll($uid = '', $tid = '')
    {
        $map['isdel'] = 0;
        if (!empty($uid)) $map['uid'] = $uid;
        if (!empty($tid)) $map['tid'] = $tid;
        $field = 'id,tid,uid,touid,floorid,content,best,zan,reply_time';
        $list = $this->field($field)->where($map)->order('reply_time desc')->select();
        $forum_model = new Forum();
        $user_model = new User();
        foreach ($list as $key => $val) {
            $t_uid = $forum_model->search(['id' => $val['tid']], 'uid');
            $list[$key]['ask'] = 0;
            if ($val['uid'] == $t_uid['uid']) {
                $list[$key]['ask'] = 1;
            }
            $list[$key]['content'] = substr_ext($val['content'], 0, 35);
            $list[$key]['title'] = substr_ext($forum_model->getField($val['tid'], 'title'), 0, 24);
            $list[$key]['username'] = $user_model->getUserField($val['uid'], 'username');
            $list[$key]['add_time'] = wordTime($val['reply_time']);
            $list[$key]['best'] = $val['best'] == 1 ? '是' : '否';
            $list[$key]['ask'] = $val['ask'] == 1 ? '是' : '否';
        }
        unset($forum_model);
        unset($user_model);
        return $list;
    }

    public function getField($id, $field)
    {
        $data = $this->field($field)->where('id', $id)->find();
        return $data[$field];
    }
}