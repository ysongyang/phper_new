<?php
/**
 * @Notes 会员等级模型.
 * @author: ysongyang <ysongyang@qq.com>
 * @link
 * @copyright: copyright 2016 ysongyang all rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace app\model;

use think\Model;

class Level extends Model
{
    //获取等级列表
    public function getListAll()
    {
        $list = $this->order('sort asc')->select();
        return $list;
    }
}