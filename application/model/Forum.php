<?php
/**
 * @Notes 帖子模型.
 * @author: ysongyang <ysongyang@qq.com>
 * @link
 * @copyright: copyright 2016 ysongyang all rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace app\model;

use think\Model;

class Forum extends Model
{
    protected $autoWriteTimestamp = 'datetime';

    public function add($data)
    {
        return $this->save($data);
    }

    /**
     * 获取列表
     * @param $page
     * @return array|\PDOStatement|string|\think\Collection
     */
    public function forumlist($wheretype, $nowpage)
    {
        switch ($wheretype) {
            case "unsolved": //未结贴
                $where = "status=1 and accept=0";
                break;
            case "solved": //已采纳
                $where = "status=1 and accept=1";
                break;
            case "wonderful": //精华
                $where = "status=1 and elite=1";
                break;
            default:
                $where = "status=1";
        }
        $limits = 15;
        $filed = 'id,uid,title,top,elite,accept,color,add_time,view,cate_id';
        $count = $this->where($where)->count();
        $allpage = ceil($count / $limits);
        $allpage = intval($allpage);
        $list = $this->where($where)->order('top desc,add_time desc')->page($nowpage, $limits)->field($filed)->select();
        $info = array('lists' => $list, 'allpage' => $allpage, 'nowpage' => $nowpage);
        return $info;
    }

    /**
     * 获取内容视图
     * @param $tid
     * @return array|false|\PDOStatement|string|Model
     */
    public function view($tid)
    {
        $filed = 'id,uid,title,top,elite,accept,color,add_time,experience,content,view,cate_id';
        $data = $this->field($filed)->find($tid);
        return $data;
    }

    /**
     * 更新浏览量
     * @param $tid
     * @return int|true
     * @throws \think\Exception
     */
    public function updateView($tid)
    {
        return $this->where('id', $tid)->setInc('view');
    }

    /**
     * @param $where
     * @param string $filed
     * @param bool $Condition
     * @return array|false|\PDOStatement|string|\think\Collection|Model
     */
    public function search($where, $filed = "*", $Condition = true)
    {
        if ($Condition) {
            $list = $this->where($where)->field($filed)->findOrFail();
        } else {
            $list = $this->where($where)->field($filed)->select();
        }
        //echo User::getLastSql();
        return $list;

    }


    /**
     *更新回复表‘赞’值
     * @param int $id 回复id
     * @param $type 1增/0减
     */
    public function UpdateLike($id, $type)
    {
        if ($type == 1) {
            db('forum_reply')->where('id', $id)->setInc('like', 1);
            echo db('forum_reply')->getLastSql();
        } else {
            db('forum_reply')->where(['id' => $id])->setDec('like', 1);
        }
    }

    /**
     * 取主题列表
     * @param $id
     * @param $uid
     * @param array $condition
     * @return false|\PDOStatement|string|\think\Collection
     */
    public function getListAll($id, $uid = '', $condition = '')
    {
        $map = array();
        if (!empty($id)) $map['id'] = $id;
        if (!empty($uid)) $map['uid'] = $uid;
        if (!empty($condition)) $map['id'] = ['neq',$condition];
        $list = $this->where($map)->order('add_time desc')->select();
        foreach ($list as $key => $val) {
            $list[$key]['username'] = getByUserInfo($val['uid'], 'username');
            $list[$key]['catename'] = getCageName($val['cate_id'], 'name');
            $list[$key]['reply'] = getReplayCount($val['id']);
            if (empty($id)) {
                $list[$key]['add_time'] = wordTime($val['add_time']);
                $list[$key]['status'] = $val['status'] == 1 ? '正常' : '<span class="text-danger">删除</span>';
            }
        }
        return $list;
    }

    public function getField($id, $field)
    {
        $data = $this->field($field)->where('id', $id)->find();
        return $data[$field];
    }
}