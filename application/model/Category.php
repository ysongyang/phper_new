<?php

namespace app\model;

use think\Model;

class Category extends Model
{

    /**
     * 反转义HTML实体标签
     * @param $value
     * @return string
     */
    protected function setContentAttr($value)
    {
        return htmlspecialchars_decode($value);
    }


    /**
     * @return array
     */
    public function getCategoryTree(){
        $data = $this->field('id,name,pid')->order('sort asc')->select();
        return get_arrayToTree($data,0);
    }

    /**
     * 获取层级缩进列表数据
     * @return array
     */
    public function getLevelList()
    {
        $category_level = $this->order(['sort' => 'DESC', 'id' => 'ASC'])->select();
        return array2level($category_level);
    }

    public function getField($id, $field)
    {
        $data = $this->field($field)->where('id', $id)->find();
        return $data[$field];
    }

}