<?php
/**
 * 前端基类
 * User: ysongyang
 * Date: 2017/7/25
 * Time: 16:40
 */
namespace app\index\controller;

use think\Controller;
use think\Session;
use think\Db;

header("Content-type:text/html;charset=utf-8");

class Base extends Controller{

    protected $uid;
    protected $userinfo;
    protected $system;

    function _initialize()
    {
        $this->uid = Session::get('userinfo.id');
        if(!empty($this->uid)){
            $filed="id,openid,sina_openid,email,username,headimgurl,level,gender,province,city,sign,experience,regdate,activated,lock";
            $user_model = new \app\model\User();
            $user= $user_model->search(['id'=>$this->uid],$filed);
            $user['gid'] = getGidByLevel($user['level']);
            $this->userinfo = $user;
            $this->assign('user',$this->userinfo);
        }
        $site_config = Db::name('system')->field('value')->where('name', 'site_config')->find();
        $site_config = unserialize($site_config['value']);
        $this->system=$site_config;
        $this -> assign('site_config', $site_config);
        $this -> assign('site_version', time());
        //$this -> assign('nav',$this->getNav());
        $this -> assign('navthis','');
        $this->assign('cachePage','user');
        $this -> assign('title','首页');
        //判断是否永久登录
        $this->checkLong();
    }

    //判断是否持久登录
    public function checkLong(){
        $user_model = new \app\model\User();
        $user =  $user_model->checkRemember();
        if($user == true){
            $userinfo=array(
                'id'=>$user['id'],
                'username'=>$user['username'],
                'headimgurl'=>$user['headimgurl'],
                'level'=>$user['level'],
                'gid'=>getGidByLevel($user['level'])
            );
            Session::set("userinfo",$userinfo);
        }
    }
    /**
     *检测用户是否登录
     */
    public function checkLogin($args=null){
        $user = Session::get('userinfo');
        if(empty($user)){
            $this->redirect('index/user/login');
        }
        /*$userInfo = \app\index\model\User::search(['id'=>$user['id']]);
        return $userInfo;*/
    }

    /**
     * 获取人类验证码的类
     */
    public function getVerify()
    {
        $list = db('verify')->select();
        $count=count($list);
        $rand = rand(0,$count-1);
        return $list[$rand];
    }

    /**
     * 获取首页导航
     */
    public function getNav()
    {
        $nav = db('nav')->where(['status'=>1])->order('sort asc')->select();
        return $nav;
    }
}