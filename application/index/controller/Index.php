<?php

namespace app\index\controller;

use app\model\Links;

class Index extends Base
{

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {

        $topList = $this->topList(); //获取置顶帖
        //$hotData =Top_Posts(); //最近热帖
        $nearList = nearList();//近期热议
        $list = $this->newList();
        $monthUser = $this->monthMember();
        $links = $this->getLinks();
        $this->assign('more', 0);
        if (count(array($list)) > 28) {
            $this->assign('more', 1);
        }
        $this->assign('monthUser', $monthUser);
        //$this->assign('hot_data',$hotData);
        $this->assign('nearList', $nearList);
        $this->assign('topList', $topList);
        $this->assign('links', $links);
        $this->assign('list', $list);
        $this->assign('cachePage', 'index');
        return $this->fetch();
    }

    //获取置顶帖
    protected function topList()
    {
        $forum_model = new \app\model\Forum();
        $where = "top = 1";
        $filed = 'id,uid,title,top,elite,color,add_time,view,cate_id';
        $list = $forum_model->where($where)->field($filed)->order("id desc")->select();
        foreach ($list as $key => $val) {
            $list[$key]['username'] = getByUserInfo($val['uid'], 'username');
            $list[$key]['avatar'] = getByUserInfo($val['uid'], 'headimgurl');
            $list[$key]['catename'] = getCageName($val['cate_id'], 'name');
            $list[$key]['reply'] = getReplayCount($val['id']);
            $list[$key]['add_time'] = wordTime($val['add_time']);
        }
        return $list;
    }

    //获取首页信息列表28条
    protected function newList()
    {
        $forum_model = new \app\model\Forum();
        $where = "top = 0 and status = 1";
        $filed = 'id,uid,title,top,elite,color,add_time,view,cate_id';
        $list = $forum_model->where($where)->field($filed)->order("id desc")->limit(28)->select();
        foreach ($list as $key => $val) {
            $list[$key]['username'] = getByUserInfo($val['uid'], 'username');
            $list[$key]['avatar'] = getByUserInfo($val['uid'], 'headimgurl');
            $list[$key]['catename'] = getCageName($val['cate_id'], 'name');
            $list[$key]['reply'] = getReplayCount($val['id']);
            $list[$key]['add_time'] = wordTime($val['add_time']);
        }
        return $list;
    }

    public function tips()
    {
        $msg = request()->param('msg');
        //$msg=iconv("gb2312","UTF-8",$msg);
        $this->assign('msg', $msg);
        return $this->fetch();
    }

    /**
     * 月度会员 前12名
     */
    public function monthMember()
    {
        $monthUser = db('forum_reply')->where(['isdel' => 0])->field("count(uid) as num,uid")->whereTime('reply_time', 'month')->group('uid')->order("num desc")->limit(12)->select();
        $newArr = array();
        if (!empty($monthUser)) {
            foreach ($monthUser as $key => $val) {
                $newArr[$key]['username'] = getByUserInfo($val['uid'], 'username');
                $newArr[$key]['avatar'] = getByUserInfo($val['uid'], 'headimgurl');
                $newArr[$key]['reply'] = getReplayUidCount($val['uid']);
                $newArr[$key]['uid'] = $val['uid'];
            }
            return $newArr;
        }
    }

    public function _404()
    {
        return $this->fetch('404');
    }

    public function getLinks()
    {
        $links_model = new Links();
        $list = $links_model->order('sort asc')->select();
        unset($links_model);
        return $list;
    }
}
