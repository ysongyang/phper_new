<?php
/**
 * 用户控制器.
 * User: ysongyang
 * Date: 2017/7/25
 * Time: 16:40
 */

namespace app\index\controller;

use app\api\controller\PHPEmail;
use app\api\controller\ResUpload;
use app\model\Forum;
use app\model\User as UserModel;
use think\Db;
use think\model\Collection;
use think\Session;
use think\Cookie;
use think\Config;

class User extends Base
{

    function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub
    }

    /**
     * 会员中心首页
     */
    public function index()
    {
        $this->assign('title', '用户中心');
        return $this->fetch();
    }

    /**
     * 登录
     */
    public function login()
    {
        if (request()->isPost()) {
            $email = request()->param('email');
            $pass = request()->param('pass');
            $keeptime = request()->param('keeptime');
            $answer = request()->param('answer');//获取人类验证的答案
            $vercode = request()->param('vercode');
            /*if (!captcha_check(request()->param('captcha'))) {
                $result = array('status' => 1, 'msg' => '验证码错误');
                return json($result);
            }*/
            if ($vercode !== $answer) {
                $result = array('status' => 1, 'msg' => "人类验证失败");
                return json($result);
            }
            if ($keeptime !== 0) {
                $salt = $salt = Config::get('salt');;
                //第二分身标识
                $identifier = md5($salt . md5($email . $salt));
                //永久登录标识
                $token = md5(uniqid(rand(), true));
                //记录时间
                $timeout = $keeptime;
            }
            $user_model = new UserModel();
            $user = $user_model->login($email, $pass);
            $where = array('email' => $email);
            $data = array('lastdate' => date('Y-m-d H:i:s'), 'lastip' => get_client_ip());
            if (!$user) {
                $result = array('status' => 1, 'msg' => '邮箱或密码错误！');
            } else {
                if (empty($user['openid']) && ($user['lock'] == 0)) {
                    $result = array('status' => 1, 'msg' => "用户已被锁定");
                    //$this->success('当前用户被锁定！', U('Login/logout'));
                } else {
                    // 更新登录次数
                    db('user')->where($where)->setInc('loginnum');
                    // 更新用户信息
                    $user_model->saveUser($where, $data);

                    $userinfo = array(
                        'id' => $user['id'],
                        'username' => $user['username'],
                        'headimgurl' => $user['headimgurl'],
                        'level' => $user['level'],
                        'gid' => getGidByLevel($user['level'])
                    );
                    Session::set('userinfo', $userinfo);
                    //Session::set('up_state', null);
                    //如果用户勾选了"记住我",则保持持久登陆
                    //存入cookie
                    Cookie::set('loginauth', "$identifier:$token", $timeout);
                    $user_model->saveRemember($user['id'], $identifier, $token, $timeout);
                    //把用户名存入cookie，退出登录后在表单保存用户名信息
                    //Cookie::set('username', $user['username'], $timeout);
                    $result = array('status' => 0, 'msg' => "登录成功", 'action' => '/user');
                    //$this->redirect('index/user/index');
                }
            }
            return json($result);
        } else {
            $user = Session::get('userinfo');
            if ($user) {
                $this->redirect('index/user/index');
            }
            $verify = $this->getVerify();
            $this->assign('verify', $verify);
            $this->assign('title', '登入');
            return $this->fetch();
        }

    }

    //退出
    public function logout()
    {
        Session::set('userinfo', null);
        Session::set('user_qq', null);
        Cookie::set('loginauth', null);
        //Cookie::set('username', null);
        $this->redirect('index/index/index');
    }

    /**
     * 注册
     */
    public function reg()
    {
        if (request()->isPost()) {
            $email = request()->param('email');
            $username = request()->param('username');
            $pass = request()->param('pass');
            $repass = request()->param('repass');
            $answer = request()->param('answer');
            $vercode = request()->param('vercode');
            $user_model = new UserModel();
            $check_em = $user_model->search(array('email' => $email));
            $check_um = $user_model->search(array('username' => array('like', "%{$username}%")));
            if ($check_em) {
                $result = array('status' => 1, 'msg' => "该邮箱已存在");
            } elseif (is_numeric($username)) {
                $result = array('status' => 1, 'msg' => "昵称不能全为数字");
            } elseif ($check_um) {
                $result = array('status' => 1, 'msg' => "该昵称已存在");
            } elseif (strlen($pass) < 6 || strlen($pass) > 16) {
                $result = array('status' => 1, 'msg' => "密码必须为6到16个字符");
            } elseif ($pass !== $repass) {
                $result = array('status' => 1, 'msg' => "两次密码并不一致");
            } elseif ($vercode !== $answer) {
                $result = array('status' => 1, 'msg' => "人类验证失败");
            } else {
                $phpEmail = new PHPEmail();
                @$send_email = $phpEmail->reg_send_mail($email, $username);
                $user = $user_model->register(request());
                if ($user) {
                    $userinfo = array(
                        'id' => $user['id'],
                        'username' => $user['username'],
                        'headimgurl' => $user['headimgurl'],
                        'level' => $user['level'],
                        'gid' => getGidByLevel($user['level'])
                    );
                    //$field=array("id","txtusername","email");
                    //$user = $this->user_db->where(array('txtUsername'=>$txtUsername))->find();
                    $keeptime = 604800; //默认一周
                    $salt = $salt = Config::get('salt');;
                    //第二分身标识
                    $identifier = md5($salt . md5($user['id'] . $salt));
                    //永久登录标识
                    $token = md5(uniqid(rand(), true));
                    //记录时间
                    $timeout = $keeptime;
                    //登录成功将用户信息存储session
                    Session::set('userinfo', $userinfo);
                    //如果用户勾选了"记住我",则保持持久登陆
                    //存入cookie
                    Cookie::set('loginauth', "$identifier:$token", $timeout);
                    //Session::set('up_state', null);
                    $user_model->saveRemember($user['id'], $identifier, $token, $timeout);
                    Session::set('reg_email', $email);
                    $result = array('status' => 0, 'msg' => "注册成功", 'action' => "/user");
                } else {
                    $result = array('status' => 1, 'msg' => "出现错误");
                }
            }
            return json($result);
        } else {
            $verify = $this->getVerify();
            $this->assign('verify', $verify);
            $this->assign('title', '注册');
            return $this->fetch();
        }
    }

    /**
     * 注册时邮箱激活
     * @return mixed
     */
    public function activate()
    {
        $activate = hex2bin(request()->param('activate')); //获取点击激活码
        $sessi_activiate = hex2bin(Session::get('activate_random'));
        //$username =$_SESSION['reg_username'];
        //$email = $_SESSION['reg_email'];
        $email = request()->param('email');
        $where = ['email' => $email];
        if (!empty($activate) && empty($sessi_activiate)) {
            $this->redirect('/tips', array('msg' => '该链接已失效，请重新注册'));
        }
        $user_model = new UserModel();
        $user = $user_model->search(['email' => $email]);
        //如果该用户存在
        if (!empty($user)) {
            if ($user['activated'] == 1) {
                $this->redirect('/tips', array('msg' => '该用户已激活'));
            } elseif ($user['activated'] == 0 && $activate == $sessi_activiate) {
                //激活成功 清楚session
                Session::set("reg_username", "");
                Session::set("reg_email", "");
                Session::set("activate_random", "");
                /*session_unset();
                session_destroy();*/
                //将用户激活
                $level = $user['level'] == -1 ? 1 : $user['level'];
                $user_model->saveUser($where, ['activated' => 1, 'level' => $level]);
                $this->redirect('/user/activate');
            }
        } else {
            $this->checkLogin();
            if (empty($this->userinfo['email'])) {
                $this->redirect('/user/set');
            }
            $this->assign('title', '激活邮箱');
            return $this->fetch();
        }
    }

    /**
     * 设置
     * @return mixed
     */
    public function set()
    {
        if (request()->isPost()) {
            $data = request()->param();
            $user = $this->userinfo;
            $user_model = new UserModel();
            switch ($data['type']) {
                case 'info':
                    /*if ($data['email'] == $user['email']) {
                        $result = array('status' => 0, 'msg' => '修改成功');
                        return json($result);
                    }*/
                    if (is_numeric($data['username'])) {
                        $result = array('status' => 1, 'msg' => "昵称不能全为数字");
                        return json($result);
                    }
                    $email = $user_model->search(array('email' => $data['email'], 'id' => array('neq', $this->uid)));
                    if (!empty($email)) {
                        $result = array('status' => 1, 'msg' => '邮箱已存在');
                        return json($result);
                    }
                    $username = $user_model->search(['username' => $data['username'], 'id' => array('neq', $this->uid)], 'username');
                    if (!empty($username)) {
                        $result = array('status' => 1, 'msg' => '已存在该昵称');
                        return json($result);
                    }
                    unset($data['type']);
                    unset($data['uid']);
                    $where['id'] = $user['id'];
                    $user_model->saveUser($where, $data);
                    //echo \app\index\model\User::getLastSql();
                    //如果修改的邮箱不等于注册邮箱 需要重新激活   || empty($user['email']) && !empty($user['openid'])
                    if ($data['email'] !== $user['email']) {
                        $phpemail = new PHPEmail();
                        $res_email = $phpemail->activate_send_mail($data['email'], $data['username']);
                        if ($res_email) {
                            $userinfo = array(
                                'id' => $user['id'],
                                'username' => $user['username'],
                                'headimgurl' => $user['headimgurl'],
                                'level' => $user['level']
                            );
                            $user_model->saveUser(['id' => $user['id']], ['activated' => 0]);
                            Session::set('userinfo', $userinfo); //更新session
                            Session::set('reg_email', $data['email']);
                            $result = array('status' => 0, 'code' => 1, 'action' => '/user/set');
                        } else {
                            $result = array('status' => 1, 'msh' => '出现错误');
                        }
                    } else {
                        $result = array('status' => 0, 'code' => 1, 'msg' => '修改成功');
                    }
                    break;
                case 'pass':
                    //修改密码需要增加一个，如果是QQ登录用户，初次修改密码怎么办？
                    /*if ((int)$data['ispassword'] !== 1) {
                        if (think_ucenter_md5($data['nowpass']) !== $user['password']) {
                            $result = array('status' => 1, 'msg' => '当前密码输入有误');
                        } elseif (trim($data['pass']) !== trim($data['repass'])) {
                            $result = array('status' => 1, 'msg' => '两次密码输入不一致');
                        } else {
                            $password = trim($data['repass']);
                            $where = array('id' => $this->uid);
                            $user_model->updatepassword($where, $password);
                            $result = array('status' => 0, 'code' => 1, 'msg' => '修改成功');
                        }
                    } else {*/
                    $email = $user_model->search(['id' => $this->uid], 'email');
                    if (empty($email['email'])) {
                        $result = array('status' => 1, 'msg' => '先验证邮箱后才可设置密码');
                    } elseif (trim($data['pass']) !== trim($data['repass'])) {
                        $result = array('status' => 1, 'msg' => '两次密码输入不一致');
                    } else {
                        $password = trim($data['repass']);
                        $where = array('id' => $this->uid);
                        $user_model->updatepassword($where, $password);
                        $result = array('status' => 0, 'code' => 1, 'msg' => '修改成功');
                    }
                    /* }*/

                    break;
            }
            return json($result);
        } else {
            $this->checkLogin();
            $this->assign('ispassword', 0);
            if (empty($this->userinfo['password']) && !empty($this->userinfo['openid']) || !empty($this->userinfo['sina_openid'])) {
                //如果是QQ注册用户，没有密码
                $this->assign('ispassword', 1);
            }
            $this->assign('title', '账号设置');
            return $this->fetch();
        }
    }

    public function message()
    {
        $this->checkLogin();
        $noticeList = Db::query('SELECT * FROM pe_forum_reply WHERE isdel=0 AND uid !=:miuid AND noticeclear=1 AND  isread=1 and tid in (SELECT id FROM pe_forum WHERE uid =:uid) OR touid=:touid  AND noticeclear=1 ORDER BY reply_time desc', ['miuid' => $this->uid, 'uid' => $this->uid, 'touid' => $this->uid]);
        $this->assign('notice', 0);
        if ($noticeList) {
            $this->assign('notice', 1);
        }
        $this->assign('title', '我的消息');
        return $this->fetch();
    }

    //附件上传
    public function upload()
    {
        $upload = new ResUpload();
        $info = $upload->upload('', 50);
        echo json_encode($info);
    }

    /**
     * 头像上传操作
     */
    public function avatar()
    {
        $uid = Session::get('userinfo.id');
        $avatar = request()->param('avatar');
        $user_model = new UserModel();
        $user_model->saveUser(['id' => $uid], ['headimgurl' => $avatar]);
    }

    /**
     * QQ绑定、QQ注册
     * @return mixed
     */
    public function qqbind()
    {
        $user_qq = Session::get("user_qq");
        $userinfo = $this->userinfo;
        $user_model = new UserModel();
        $gender = 0;
        //QQ绑定的相关操作
        if (!empty($userinfo)) {
            $user = $user_model->search(['openid' => $user_qq['openid']]);
            if ($user) {
                $this->redirect('index/index/tips', array('msg' => '绑定失败,已绑定其他账号！'));
            }
            if ($user_qq['gender'] == '男') {
                $gender = 1;
            } elseif ($user_qq['gender'] == '女') {
                $gender = 2;
            }
            $data = array(
                'openid' => $user_qq['openid'],
                //'username' => $user_qq['username'],
                //'headimgurl' => $user_qq['headimgurl'],
                'gender' => $gender //性别
            );
            $where = array(
                'id' => $userinfo['id']
            );
            $res = $user_model->saveUser($where, $data);
            if ($res) {
                $this->redirect('index/user/set#bind');
            }
        } else {    //QQ新用户注册的相关操作
            if ($user_qq['gender'] == '男') {
                $gender = 1;
            } elseif ($user_qq['gender'] == '女') {
                $gender = 2;
            }
            $cityInfo = GetIpLookup(get_client_ip());
            $headimgurl = config('upload_path') . 'default/avatar/' . rand(0, 41) . '.jpg';
            //$headimgurl = substr($headimgurl, 1, strlen($headimgurl));
            //QQ注册时如果昵称存在则需要增加随机数
            if ($user_model->search(['username' => $user_qq['nickname']])) {
                $user_qq['nickname'] = $user_qq['nickname'] . str_rand(4);
            }
            $data = array(
                'openid' => $user_qq['openid'],
                'headimgurl' => !empty($user_qq['headimage']) ? $user_qq['headimage'] : $headimgurl,
                'username' => $user_qq['nickname'],
                'gender' => $gender, //性别
                'country' => $cityInfo['country'],
                'province' => $cityInfo['province'],
                'city' => $cityInfo['city'],
                'regdate' => date('Y-m-d H:i:s'),
                'regip' => get_client_ip(),
                'lastdate' => date('Y-m-d H:i:s'),
                'lastip' => get_client_ip()
            );
            if ($user_model->qqregister($data)) {
                $user = $user_model->search(['openid' => $user_qq['openid']]);
                $info = array(
                    'id' => $user['id'],
                    'username' => $user['username'],
                    'headimgurl' => $user['headimgurl'],
                    'level' => $user['level'],
                    'gid' => getGidByLevel($user['level'])
                );
                //$field=array("id","txtusername","email");
                //$user = $this->user_db->where(array('txtUsername'=>$txtUsername))->find();
                $keeptime = 604800; //默认一周
                $salt = $salt = Config::get('salt');;
                //第二分身标识
                $identifier = md5($salt . md5($user['openid'] . $salt));
                //永久登录标识
                $token = md5(uniqid(rand(), true));
                //记录时间
                $timeout = $keeptime;
                //登录成功将用户信息存储session
                Session::set('userinfo', $info);
                //如果用户勾选了"记住我",则保持持久登陆
                //存入cookie
                Cookie::set('loginauth', "$identifier:$token", $timeout);
                //Session::set('up_state', null);
                Session::set('user_qq', null); //将QQ登录的session清空
                $user_model->saveRemember($user['id'], $identifier, $token, $timeout);
                $this->redirect('index/user/set');
                //$this->success('注册成功,将为您自动登录！', U('UserInfo/index'), 2);
            } else {
                $this->redirect('index/user/login');
                //$this->error('注册失败');
            }
        }
    }

    /**
     * SINA绑定、SINA注册
     * @return mixed
     */
    public function sinabind()
    {
        $user_sina = Session::get("user_sina");
        $userinfo = Session::get("userinfo");
        $user_model = new UserModel();
        if (!empty($userinfo)) {//新浪绑定的相关操作
            $user = $user_model->search(['sina_openid' => $user_sina['openid']]);
            if ($user) {
                $this->redirect('index/index/tips', array('msg' => '绑定失败！'));
            }
            $data = array(
                'sina_openid' => $user_sina['sina_openid'],
                'sina_token' => $user_sina['sina_token'],
                //'username' => $user_sina['screen_name'],
                //'headimage' => $user_sina['headimage'],
                'gender' => $user_sina['gender'] //性别
            );
            $where = array(
                'id' => $userinfo['id']
            );
            $res = $user_model->saveUser($where, $data);
            if ($res) {
                $this->redirect('index/user/set#bind');
            }
        } else {//新浪新注册的操作
            $cityInfo = GetIpLookup(get_client_ip());
            $headimgurl = config('upload_path') . 'default/avatar/' . rand(0, 41) . '.jpg';
            //$headimgurl = substr($headimgurl, 1, strlen($headimgurl));
            if ($user_model->search(['username' => $user_sina['username']])) {
                $user_sina['username'] = $user_sina['username'] . str_rand(4);
            }
            $data = array(
                'sina_openid' => $user_sina['sina_openid'],
                'sina_token' => $user_sina['sina_token'],
                'headimgurl' => $headimgurl,
                'username' => $user_sina['username'],
                'gender' => $user_sina['gender'], //性别
                'country' => $cityInfo['country'],
                'province' => $cityInfo['province'],
                'city' => $cityInfo['city'],
                'regdate' => date('Y-m-d H:i:s'),
                'regip' => get_client_ip(),
                'lastdate' => date('Y-m-d H:i:s'),
                'lastip' => get_client_ip()
            );
            if ($user_model->sinaregister($data)) {
                $user = $user_model->search(['sina_openid' => $user_sina['sina_openid']]);
                $userinfo = array(
                    'id' => $user['id'],
                    'username' => $user['username'],
                    'headimgurl' => $user['headimgurl'],
                    'level' => $user['level'],
                    'gid' => getGidByLevel($user['level'])
                );
                //$field=array("id","txtusername","email");
                //$user = $this->user_db->where(array('txtUsername'=>$txtUsername))->find();
                $keeptime = 604800; //默认一周
                $salt = $salt = Config::get('salt');;
                //第二分身标识
                $identifier = md5($salt . md5($user['sina_openid'] . $salt));
                //永久登录标识
                $token = md5(uniqid(rand(), true));
                //记录时间
                $timeout = $keeptime;
                //登录成功将用户信息存储session
                Session::set('userinfo', $userinfo);
                //如果用户勾选了"记住我",则保持持久登陆
                //存入cookie
                Cookie::set('loginauth', "$identifier:$token", $timeout);
                //Session::set('up_state', null);
                Session::set('user_sina', null); //将QQ登录的session清空
                $user_model->saveRemember($user['id'], $identifier, $token, $timeout);
                $this->redirect('index/user/set');
                //$this->success('注册成功,将为您自动登录！', U('UserInfo/index'), 2);
            } else {
                $this->redirect('index/user/login');
                //$this->error('注册失败');
            }
        }
    }

    /**
     * QQ解除绑定
     */
    public function unbind()
    {
        $type = request()->param("type");
        switch ($type) {
            case 'qq_id':
                $data = array(
                    'openid' => '',
                );
                break;
            case 'weibo_id':
                $data = array(
                    'sina_openid' => '',
                    'sina_token' => '',
                );
                break;
        }
        $user_model = new UserModel();
        $where = array('id' => Session::get('userinfo.id'));
        $res = $user_model->saveUser($where, $data);
        if ($res) {
            $result = array('status' => 0, 'msg' => "解除绑定成功");
        } else {
            $result = array('status' => 1, 'msg' => "解除绑定失败");
        }
        return json($result);
    }

    /**
     * 个人主页
     * @return mixed
     */
    public function home()
    {
        $this->checkLogin();
        $uid = request()->param('uid');
        $user_model = new UserModel();
        $forum_model = new Forum();
        $user_model->insertGuest($this->uid, $uid);
        $user = $user_model->search(['id' => $uid], "*");
        //获得当前我的提问
        $list = $forum_model->search(['uid' => $uid, 'status' => 1], "id,uid,title,elite,view,add_time", false);
        foreach ($list as $key => $val) {
            $list[$key]['reply'] = getReplayCount($val['id']);
        }
        //获得当前我的回答
        $reply = db('forum_reply')->where(['uid' => $uid, 'isdel' => 0])->field("id,tid,floorid,content,reply_time")->order("reply_time desc")->limit(15)->select();
        foreach ($reply as $key => $val) {
            $forum = $forum_model->search(['id' => $val['tid']], "title");
            $reply[$key]['title'] = $forum['title'];
            $reply[$key]['content'] = reply_replace_face($val['content']);
        }
        $this->assign('user', $user);
        $this->assign('list', $list);
        $this->assign('reply', $reply);
        return $this->fetch();
    }

    /**
     * 找回密码 / 重置密码
     * @return mixed
     */
    public function forget()
    {
        if (request()->isPost()) {
            $email = request()->param('email');
            $answer = request()->param('answer');
            $vercode = request()->param('vercode');
            $user_model = new UserModel();
            $result = array();
            $user = $user_model->search(['email' => $email]);
            if (empty($user)) {
                $result = array('status' => 1, 'msg' => '账号不存在');
            } elseif ($answer !== $vercode) {
                $result = array('status' => 1, 'msg' => '人类验证错误');
            } else {
                $phpEmail = new PHPEmail();
                $check = $phpEmail->getpass_send_mail($email, $user['username']);
                if ($check) {
                    $result = array('status' => 0, 'msg' => "已成功将链接发送到了您的邮箱，接受可能会稍有延迟，请注意查收。");
                }
            }
            return json($result);
        } else {
            $user = Cookie::get('userinfo');
            if ($user) {
                $this->redirect('index/user/index');
            }
            $verify = $this->getVerify();
            $this->assign('verify', $verify);
            $this->assign('title', '找回密码/重置密码');
            return $this->fetch();
        }

    }

    //重置您的密码
    public function forset()
    {
        $user_model = new UserModel();
        if (request()->isPost()) {
            $data = request()->param();
            $answer = request()->param('answer');
            $vercode = request()->param('vercode');
            if (strlen($data['pass']) < 6 || strlen($data['pass']) > 16) {
                $result = array('status' => 1, 'msg' => "密码必须为6到16个字符");
            } elseif ($data['pass'] !== $data['repass']) {
                $result = array('status' => 1, 'msg' => "两次密码并不一致");
            } elseif ($vercode !== $answer) {
                $result = array('status' => 1, 'msg' => "人类验证失败");
            } else {
                $password = trim($data['repass']);
                $where = array('email' => $data['email']);
                $user_model->updatepassword($where, $password);
                Session::set('for_email', null);
                //Session::set('up_state', 1);
                $result = array('status' => 0, 'msg' => '修改成功', 'action' => '/user/login');
            }
            return json($result);
        } else {
            $user = Session::get('userinfo');
            if ($user) {
                $this->redirect('index/user/index');
            }
            /*$up_state = Session::get('up_state');
            if (!$up_state) {
                $this->redirect('index/user/login');
            }*/
            $email = Session::get('for_email');
            if (empty($email)) {
                $this->redirect('index/user/login');
            }
            $user = $user_model->search(['email' => $email]);
            $verify = $this->getVerify();
            $this->assign('verify', $verify);
            $this->assign('set_user', $user);
            $this->assign('title', '找回密码/重置密码');
            return $this->fetch();
        }

    }

    /**
     * 重置密码
     * @return mixed
     */
    public function forgetpass()
    {
        $key = hex2bin(request()->param('key')); //获取点击激活码
        $session_key = hex2bin(Session::get('key_random'));
        $email = request()->param('email');
        $user_model = new UserModel();
        if (empty($session_key)) {
            $this->redirect('index/index/tips', array('msg' => '该重置密码链接已失效，请重新校验您的信息'));
        }
        if ($key == $session_key) {
            $user = $user_model->search(['email' => $email]);
            //如果该用户存在
            if (!empty($user)) {
                Session::set('for_email', $email);
                //验证成功 清楚session
                Session::set("key_random", null);
                $this->redirect('index/user/forset');
            }
        }
    }

    //点击用户名跳转
    public function jump()
    {
        header("Content-type:text/html;charset=utf-8");
        $username = request()->param('username');
        if (!preg_match('/^.*$/u', $username)) {
            $username = iconv('GBK', 'UTF-8', $username); //将字符串string  编码由gbk转变成utf8；
        }
        $user_model = new UserModel();
        $uid = $user_model->where('username', 'like', '%' . $username . '%')->field("id")->find();
        /* echo $user_model->getLastSql();
         dump($uid);
         die;*/
        if (empty($uid)) {
            $this->redirect(url('index/index/_404'));
        }
        //echo model('user')->getLastSql();
        //echo $uid['id'];
        $this->redirect(url('index/user/home', ['uid' => $uid['id']]));
    }

    //获取我发布的帖子
    public function myPost()
    {
        $page = request()->param('page');
        $pnums = request()->param('pnums');
        $where = "uid ={$this->uid} and status=1";
        $forum_model = new Forum();
        $filed = 'id,uid,title,accept,top,elite,color,add_time,view,cate_id,status,experience';
        $count = $forum_model->where($where)->field($filed)->order("top desc,add_time desc")->count();
        $list = $forum_model->where($where)->field($filed)->order("top desc,add_time desc")->paginate($pnums, false, ['page' => $page]);
        $result['status'] = 0;
        $result['count'] = $count;
        $result['pages'] = ceil($count / (int)$pnums);
        $result['rows'] = [];
        foreach ($list as $key => $val) {
            $result['rows'][$key]['id'] = $val['id'];
            $result['rows'][$key]['title'] = $val['title'];
            $result['rows'][$key]['uid'] = $val['uid'];
            $result['rows'][$key]['accept'] = $val['accept'];
            $result['rows'][$key]['hits'] = $val['view'];
            $result['rows'][$key]['catename'] = getCageName($val['cate_id'], 'name');
            $result['rows'][$key]['comment'] = getReplayCount($val['id']);
            $result['rows'][$key]['time'] = wordTime($val['add_time'], true);
            $result['rows'][$key]['elite'] = $val['elite'];
            $result['rows'][$key]['top'] = $val['top'];
            $result['rows'][$key]['experience'] = $val['experience'];
        }
        return json($result);
    }

    //获取我收藏的帖子
    public function myCollection()
    {
        $page = request()->param('page');
        $pnums = request()->param('pnums');
        $collection_model = new \app\model\Collection();
        //$list = $collection_model->where($where)->order('add_time desc')->select();
        $list = $collection_model->alias('coll')
            ->join('pe_forum pf', 'pf.id=coll.tid', 'LEFT')
            ->field("pf.id,pf.uid,	pf.title,pf.accept,pf.top,pf.cate_id,pf.elite,coll.add_time as collection_time,pf.view,pf.cate_id,pf.experience,pf.add_time")
            ->where("coll.uid ={$this->uid} and pf.status = 1")
            ->select();
        $result['status'] = 0;
        $result['count'] = count($list);
        $result['pages'] = ceil(count($list) / $pnums);
        $result['rows'] = [];
        foreach ($list as $key => $val) {
            $result['rows'][$key]['id'] = $val['id'];
            $result['rows'][$key]['title'] = $val['title'];
            $result['rows'][$key]['uid'] = $val['uid'];
            $result['rows'][$key]['accept'] = $val['accept'];
            $result['rows'][$key]['hits'] = $val['view'];
            $result['rows'][$key]['catename'] = getCageName($val['cate_id'], 'name');
            $result['rows'][$key]['comment'] = getReplayCount($val['id']);
            $result['rows'][$key]['collection_time'] = wordTime($val['collection_time'], true);
            $result['rows'][$key]['collection_timestamp'] = strtotime($val['collection_time']);
            $result['rows'][$key]['time'] = strtotime($val['add_time']);
            $result['rows'][$key]['elite'] = $val['elite'];
            $result['rows'][$key]['top'] = $val['top'];
            $result['rows'][$key]['experience'] = $val['experience'];
        }
        return json($result);
    }
}