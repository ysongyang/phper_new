<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;

//================= 前端路由配置 开始==================//
//首页
Route::rule('/', 'index/index/index', 'GET');
Route::rule('forum/[:type]', 'index/forum/index');
Route::rule('forum/add', 'index/forum/add');
Route::rule('detail/:tid', 'index/forum/detail', 'GET', [], ['tid' => '\d+']);
Route::any('detail/edit/:tid', 'index/forum/edit', ['method' => 'get,post'], ['tid' => '\d+']);
Route::rule('forum/reply', 'index/forum/reply', 'POST');
Route::any('forum/getDa', 'index/forum/getDa', ['method' => 'post']);
Route::any('forum/getindex', 'index/forum/getindex', ['method' => 'post,get']);
Route::any('forum/updateDa', 'index/forum/updateDa', ['method' => 'post']);
Route::any('forum/jieda_delete', 'index/forum/jieda_delete', ['method' => 'post']);
Route::any('forum/jieda_zan', 'index/forum/jieda_zan', ['method' => 'post']);
Route::any('forum/jie_delete', 'index/forum/jie_delete', ['method' => 'post']);
Route::any('forum/jie_set', 'index/forum/jie_set', ['method' => 'post']);
Route::any('forum/jieda_accept', 'index/forum/jieda_accept', ['method' => 'post']);
Route::rule('collection/[:type]', 'index/forum/collection');
Route::rule('collection/find', 'index/forum/collectionFind');
//会员
Route::rule('user', 'index/user/index', 'GET|POST');
Route::rule('user/login', 'index/user/login');
Route::rule('user/logout', 'index/user/logout');
Route::rule('user/set', 'index/user/set');
Route::rule('user/reg', 'index/user/reg');
Route::rule('user/message', 'index/user/message');
Route::rule('u/:uid', 'index/user/home', 'GET', [], ['uid' => '\d+']);
Route::any('user/upload', 'index/user/upload', ['method' => 'post']);
Route::any('user/avatar', 'index/user/avatar', ['method' => 'post']);
Route::rule('user/forget', 'index/user/forget', 'GET|POST');
Route::rule('user/forset', 'index/user/forset', 'GET|POST');
Route::rule('user/forgetpass', 'index/user/forgetpass', 'GET|POST');
Route::rule('user/bind', 'index/user/qqbind', 'GET|POST');
Route::rule('user/sinabind', 'index/user/sinabind', 'GET|POST');
Route::rule('api/unbind', 'index/user/unbind', 'GET|POST');
Route::rule('api/mine_post', 'index/user/myPost', 'GET|POST');
Route::rule('api/mine_collection', 'index/user/myCollection', 'GET|POST');
Route::rule('sendemail', 'index/user/sendemail');
//提示页
Route::rule('tips/msg/:msg', 'index/index/tips', 'GET');
Route::rule('404', 'index/index/_404', 'GET');
Route::any('jump/:username', 'index/user/jump', ['method' => 'get,post']);
//邮箱激活
Route::rule('user/activate', 'index/user/activate', 'GET');
//API重新发送邮箱
Route::any('api/activate', 'index/api/activate', ['method' => 'post']);
Route::any('api/msg_count', 'index/api/msg_count', ['method' => 'get,post']);
Route::any('api/msg_read', 'index/api/msg_read', ['method' => 'get,post']);
Route::any('api/msg', 'index/api/msg', ['method' => 'get,post']);
Route::any('api/msg_del', 'index/api/msg_del', ['method' => 'post']);
Route::any('api/mine_froum', 'index/api/mine_froum', ['method' => 'get,post']);
Route::any('api/upload', 'index/api/upload', ['method' => 'post']);
//QQ登录
Route::any('oauth/type/:type', 'index/oauth/index', ['method' => 'get,post']);
Route::any('oauth/callback/type/:type', 'index/oauth/callback', ['method' => 'get,post']);
//================= 前端路由配置 结束==================//
