<?php
namespace app\api\controller;

use think\Controller;
use think\Session;

class Upload extends Controller {
    protected function _initialize() {
        parent::_initialize();
        if(!Session::has('admin_id')){
            $this->error('未登录');
        }
    }

    public function upload($up_path='attachment/') {
        $file = $this->request->file('file');
        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads/'.$up_path);
        if ($info) {
            $imgpath =  config('upload_path').$up_path . str_replace('\\', '/', $info->getSaveName());
            if(checkIsImage($imgpath)){
                echo json(['msg'=>'非法图片']);
            }
            $result = [
                'code' => 0,
                'msg'  => '上传成功',
                'data' => [
                    'src'   => $imgpath,
                    'title' => ''
                ]
            ];
        } else {
            $result = [
                'code' => -1,
                'msg'  => $file->getError()
            ];
        }

        return json($result);
    }

    /**
     * 上传缩略图
     * @return \think\response\Json
     */
    public function uploadThumb($up_path='thumb/') {
        $file = $this->request->file('file');
        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads/'.$up_path);
        if ($info) {
            $imgpath =  config('upload_path').$up_path . str_replace('\\', '/', $info->getSaveName());
            if(checkIsImage($imgpath)){
                echo json(['msg'=>'非法图片']);
            }
            $result = [
                'code'     => 0,
                'msg'      => '上传成功',
                'filename' => $imgpath,
            ];
        } else {
            $result = [
                'code' => -1,
                'msg'  => $file->getError()
            ];
        }
        return json($result);
    }

    /**
     * 上传头像
     * @return \think\response\Json
     */
    public function uploadAvatar($up_path='avatar/') {
        $file = $this->request->file('file');
        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads/' . $up_path);
        if ($info) {
            $imgpath = '/uploads/' . $up_path . str_replace('\\', '/', $info->getSaveName());
            if (checkIsImage($imgpath)) {
                $result = [
                    'code' => -1,
                    'msg' => '非法图片',
                    'filename' => $imgpath,
                ];
                echo json_encode($result);
            }
            $result = [
                'code' => 1,
                'msg' => '上传成功',
                'filename' => $imgpath,
            ];
        } else {
            $result = [
                'code' => -1,
                'msg' => $file->getError()
            ];
        }
        echo json_encode($result);
    }

    /**
     * 上传图集
     * @return \think\response\Json
     */
    public function uploadPhoto($up_path='attachment/') {
        $files  = request()->file('photo');
        $result = [];
        foreach ($files as $file) {
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads/'.$up_path);
            if ($info) {
                $imgpath =  config('upload_path').$up_path . str_replace('\\', '/', $info->getSaveName());
                if(checkIsImage($imgpath)){
                    echo json(['msg'=>'非法图片']);
                }
                $result[] = [
                    'code' => 0,
                    'msg'  => '上传成功',
                    'data' => [
                        'filename' => $imgpath,
                        'title'    => ''
                    ]
                ];
            } else {
                $result[] = [
                    'code' => -1,
                    'msg'  => $file->getError()
                ];
            }
        }

        return json($result);
    }
}