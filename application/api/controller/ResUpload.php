<?php
/**
 * @Notes 前端上传类.
 * @author: ysongyang <ysongyang@qq.com>
 * @link
 * @copyright: copyright 2016 ysongyang all rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
namespace app\api\controller;

use app\index\controller\Base;

class ResUpload extends Base
{
    /**
     * 前端图片上传方法
     * @param string $up_path  上传路径
     * @param int $imgSize      上传图片大小(k)
     * @return mixed
     */
    public function upload($up_path='',$imgSize = 100)
    {
        if (!request()->isPost()){
            $return['msg']='非法请求';
        }elseif(empty($this->userinfo)){
            $return['msg']='请先登录';
        }else{
            // 获取表单上传文件
            $files = request()->file('file');
            if($files->getSize()/1000 > $imgSize){//判断图片大小，超出尺寸则进行删除操作
                $return['msg']='图片大小超出,图片限制'.$imgSize.'k';
                return $return;
            }
            if(empty($up_path))$up_path = 'avatar/';
            // 移动到框架应用根目录/public/uploads/ 目录下
            $info = $files->move(ROOT_PATH . 'public' . DS . 'uploads/'.$up_path);
            if ($info) {
                $path = str_replace(ROOT_PATH, '', $info->getSaveName());
                $path = str_replace('\\', '/', $path);
                $imgpath = config('upload_path').$up_path.$path;
                if(checkIsImage($imgpath)){
                    $return['msg']='非法图片';
                }else{
                    // 成功上传后 获取上传信息
                    // {"code":0 ,"msg":"","url":"http://cdn.abc.com/123.jpg"'}
                    $return['status']=0;
                    $return['url']= $imgpath;
                }
            } else {
                $return['msg'] = $files->getError();
            }
        }
        return $return;
    }

    public function upload_attachment($up_path='attachment/')
    {
        if (!request()->isPost()){
            $return['status']=1;
            $return['msg']='非法请求';
        }elseif(empty($this->userinfo)){
            $return['status']=1;
            $return['msg']='请先登录';
        }else{
            // 获取表单上传文件
            $files = request()->file('file');
            // 移动到框架应用根目录/public/uploads/ 目录下
            $info = $files->move(ROOT_PATH . 'public' . DS . 'uploads/'.$up_path);
            if ($info) {
                // 成功上传后 获取上传信息
                // {"code":0 ,"msg":"","url":"http://cdn.abc.com/123.jpg"'}
                $path = str_replace(ROOT_PATH, '', $info->getSaveName());
                $path = str_replace('\\', '/', $path);
                $imgpath = config('upload_path').$up_path.$path;
                if(checkIsImage($imgpath)){
                    $return['status']=1;
                    $return['msg']='非法图片';
                }else{
                    $return['status']=0;
                    $return['msg']='上传成功';
                    $return['url']= $imgpath;
                }
            } else {
                $return['status']=1;
                $return['msg'] = $files->getError();
            }
        }
        return $return;
    }
}
