<?php
/**
 * @Notes 邮件发送类.
 * @author: ysongyang <ysongyang@qq.com>
 * @copyright: copyright 2017.07 ysongyang all rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace app\api\controller;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use app\index\controller\Base;
use think\Session;

class PHPEmail extends Base
{

    /**
     * 系统邮件发送函数
     * @param string $address 接收邮件者邮箱
     * @param string $subject 邮件主题
     * @param string $body 邮件内容
     * @param string $attachment 附件列表
     * @return boolean
     */
    public function send_email($address, $subject = 'PHPer社区欢迎信！', $body = '', $attachment = null)
    {
        $mail = new PHPMailer(true);
        try {
            $mail->CharSet = 'UTF-8'; //设定邮件编码，默认ISO-8859-1，如果发中文此项必须设置，否则乱码
            // 服务器设置
            $mail->SMTPDebug = 0;                                    // 开启Debug
            $mail->isSMTP();                                        // 使用SMTP
            $mail->Host = config('THINK_EMAIL.SMTP_HOST');                       // 服务器地址
            $mail->SMTPAuth = true;                                    // 开启SMTP验证
            $mail->Username = config('THINK_EMAIL.SMTP_USER');                // SMTP 用户名（你要使用的邮件发送账号）
            $mail->Password = config('THINK_EMAIL.SMTP_PASS');                                // SMTP 密码
            $mail->SMTPSecure = 'ssl';                                // 开启TLS 可选
            $mail->Port = config('THINK_EMAIL.SMTP_PORT');                                        // 端口
            // 收件人
            $mail->setFrom(config('THINK_EMAIL.FROM_EMAIL'), config('THINK_EMAIL.FROM_NAME'));            // 来自
            $mail->addAddress($address);                        // 可以只传邮箱地址
            $mail->isHTML(true);
            $mail->AltBody = "text/html";    // 设置邮件格式为HTML
            $mail->Subject = $subject;      // 'PHPer社区欢迎信！'
            $mail->Body = $body;
            if (is_array($attachment)) { // 添加附件
                foreach ($attachment as $file) {
                    is_file($file) && $mail->AddAttachment($file);
                }
            }
            $mail->send();
            return true;
        } catch (Exception $e) {
            return $mail->ErrorInfo;
        }
    }


    //邮箱发送验证-注册
    public function reg_send_mail($email, $username, $subject = "PHPer社区欢迎信！")
    {
        //生成6位激活码
        $random = bin2hex(create_token());
        $domain = $this->system['site_copyright'];
        $web_name = $this->system['site_title'];
        //生成激活码模块地址
        $url = $domain . url('/user/activate') . "?activate=$random&email=$email";
        //将邮件地址和随机数放入session  并保存12个小时
        Session::set("activate_random", $random);
        //发送邮件
        //$user= $this->user_db ->where("txtusername={$username}")->find();
        set_time_limit(0);
        $content = '<table border="0" cellpadding="0" cellspacing="0" style="width: 600px; border: 1px solid #ddd; border-radius: 3px; color: #555; font-family: \'Helvetica Neue Regular\',Helvetica,Arial,Tahoma,\'Microsoft YaHei\',\'San Francisco\',\'微软雅黑\',\'Hiragino Sans GB\',STHeitiSC-Light; font-size: 12px; height: auto; margin: auto; overflow: hidden; text-align: left; word-break: break-all; word-wrap: break-word;"> <tbody style="margin: 0; padding: 0;"> <tr style="background-color: #393D49; height: 60px; margin: 0; padding: 0;"> <td style="margin: 0; padding: 0;"> <div style="color: #5EB576; margin: 0; margin-left: 30px; padding: 0;"><a style="font-size: 14px; margin: 0; padding: 0; color: #5EB576; text-decoration: none;" href="http://zz1.com.cn/" target="_blank">' . $web_name . ' - PHP程序猿分享撸码的那些事儿</a></div> </td> </tr> <tr style="margin: 0; padding: 0;"> <td style="margin: 0; padding: 30px;"> <p style="line-height: 20px; margin: 0; margin-bottom: 10px; padding: 0;"> 伟大的程序员，<em style="font-weight: 700;">' . $username . '</em>： </p> <p style="line-height: 2; margin: 0; margin-bottom: 10px; padding: 0;"> 欢迎你加入<em>PHPer社区</em>，感谢你选择我们。请点击下面的按钮激活邮箱。<span style="color: #c5b73b;">（60分钟内点击有效）</span> </p> <div style=""> <a href="' . $url . '" style="background-color: #009E94; color: #fff; display: inline-block; height: 32px; line-height: 32px; margin: 0 15px 0 0; padding: 0 15px; text-decoration: none;" target="_blank">立即激活邮箱</a> </div> <p style="line-height: 20px; margin-top: 20px; padding: 10px; background-color: #f2f2f2; font-size: 12px;"> 如果该邮件不是由你本人操作，请勿进行激活！否则你的邮箱将会被他人绑定。 </p> </td> </tr> <tr style="background-color: #fafafa; color: #999; height: 35px; margin: 0; padding: 0; text-align: center;"> <td style="margin: 0; padding: 0;">系统邮件，请勿直接回复。</td> </tr> </tbody> </table>';//邮件主体内容;

        if ($this->send_email($email, $subject, $content, $attachment = null)) {
            return true;
        } else {
            return false;
        }
    }


    //邮箱发送验证-社区激活邮件
    public function activate_send_mail($email, $username, $subject = "PHPer社区激活邮件！")
    {
        //生成6位激活码
        $random = bin2hex(create_token());
        $domain = $this->system['site_copyright'];
        $web_name = $this->system['site_title'];
        //生成激活码模块地址
        $url = $domain . url('/user/activate') . "?activate=$random&email=$email";
        //将邮件地址和随机数放入session  并保存12个小时
        Session::set("activate_random", $random);
        //发送邮件
        //$user= $this->user_db ->where("txtusername={$username}")->find();
        set_time_limit(0);
        $content = '<table style="BORDER-BOTTOM: #ddd 1px solid; TEXT-ALIGN: left; BORDER-LEFT: #ddd 1px solid; MARGIN: auto; WIDTH: 600px; FONT-FAMILY: \'Helvetica Neue Regular\', Helvetica, Arial, Tahoma, \'Microsoft YaHei\', \'San Francisco\', \'微软雅黑\', \'Hiragino Sans GB\', STHeitiSC-Light; WORD-WRAP: break-word; HEIGHT: auto; COLOR: #555; FONT-SIZE: 12px; OVERFLOW: hidden; WORD-BREAK: break-all; BORDER-TOP: #ddd 1px solid; BORDER-RIGHT: #ddd 1px solid; border-radius: 3px" border="0" cellspacing="0" cellpadding="0">
<tbody style="PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px">
<tr style="PADDING-BOTTOM: 0px; BACKGROUND-COLOR: #393d49; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; HEIGHT: 60px; PADDING-TOP: 0px"><td style="PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px"><div style="PADDING-BOTTOM: 0px; MARGIN: 0px 0px 0px 30px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; COLOR: #5eb576; PADDING-TOP: 0px"><a style="PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; COLOR: #5eb576; FONT-SIZE: 14px; TEXT-DECORATION: none; PADDING-TOP: 0px" href="http://zz1.com.cn/" target="_blank">' . $web_name . ' - PHP程序猿分享撸码的那些事儿</a></div></td></tr><tr style="PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px">
<td style="PADDING-BOTTOM: 30px; MARGIN: 0px; PADDING-LEFT: 30px; PADDING-RIGHT: 30px; PADDING-TOP: 30px">
<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 20px; MARGIN: 0px 0px 10px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px">Hi，<em style="FONT-WEIGHT: 700">' . $username . '</em>，请完成以下操作：<span style="color: #c5b73b;">（60分钟内点击有效）</span> </p><div><a style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 32px; BACKGROUND-COLOR: #009e94; MARGIN: 0px 15px 0px 0px; PADDING-LEFT: 15px; PADDING-RIGHT: 15px; DISPLAY: inline-block; HEIGHT: 32px; COLOR: #fff; TEXT-DECORATION: none; PADDING-TOP: 0px" href="' . $url . '" target="_blank">立即激活邮箱</a> </div>
<p style="PADDING-BOTTOM: 10px; LINE-HEIGHT: 20px; BACKGROUND-COLOR: #f2f2f2; MARGIN-TOP: 20px; PADDING-LEFT: 10px; PADDING-RIGHT: 10px; FONT-SIZE: 12px; PADDING-TOP: 10px">如果该邮件不是由你本人操作，请勿进行激活！否则你的邮箱将会被他人绑定。 </p></td></tr><tr style="TEXT-ALIGN: center; PADDING-BOTTOM: 0px; BACKGROUND-COLOR: #fafafa; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; HEIGHT: 35px; COLOR: #999; PADDING-TOP: 0px">
<td style="PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; PADDING-TOP: 0px">系统邮件，请勿直接回复。</td></tr></tbody></table>';//邮件主体内容;

        if ($this->send_email($email, $subject, $content, $attachment = null)) {
            return true;
        } else {
            return false;
        }
    }

    //邮箱发送验证-找回密码
    public function getpass_send_mail($email, $username, $subject = "PHPer社区找回密码")
    {
        //生成6位激活码
        $random = bin2hex(create_token());
        $domain = $this->system['site_copyright'];
        $web_name = $this->system['site_title'];
        //生成激活码模块地址
        $url = $domain . url('/user/forgetpass') . "?key=$random&email=$email";
        //将邮件地址和随机数放入session  并保存1个小时
        Session::set("key_random", $random);
        //发送邮件
        //$user= $this->user_db ->where("txtusername={$username}")->find();
        set_time_limit(0);
        $content = '<table border="0" cellpadding="0" cellspacing="0" style="width: 600px; border: 1px solid #ddd; border-radius: 3px; color: #555; font-family: \'Helvetica Neue Regular\',Helvetica,Arial,Tahoma,\'Microsoft YaHei\',\'San Francisco\',\'微软雅黑\',\'Hiragino Sans GB\',STHeitiSC-Light; font-size: 12px; height: auto; margin: auto; overflow: hidden; text-align: left; word-break: break-all; word-wrap: break-word;"> <tbody style="margin: 0; padding: 0;"> <tr style="background-color: #393D49; height: 60px; margin: 0; padding: 0;"> <td style="margin: 0; padding: 0;"> <div style="color: #5EB576; margin: 0; margin-left: 30px; padding: 0;"><a style="font-size: 14px; margin: 0; padding: 0; color: #5EB576; text-decoration: none;" href="http://zz1.com.cn/" target="_blank">' . $web_name . ' - PHP程序猿分享撸码的那些事儿</a></div> </td> </tr> <tr style="margin: 0; padding: 0;"> <td style="margin: 0; padding: 30px;"> <p style="line-height: 20px; margin: 0; margin-bottom: 10px; padding: 0;"> 你好，<em style="font-weight: 700;">' . $username . '</em>&nbsp;童鞋，请在60分钟内重置您的密码：： </p> <div style=""> <a href="' . $url . '" style="background-color: #009E94; color: #fff; display: inline-block; height: 32px; line-height: 32px; margin: 0 15px 0 0; padding: 0 15px; text-decoration: none;" target="_blank">立即重置密码</a> </div> <p style="line-height: 20px; margin-top: 20px; padding: 10px; background-color: #f2f2f2; font-size: 12px;"> 如果该邮件不是由你本人操作，请勿进行激活！否则你的邮箱将会被他人绑定。 </p> </td> </tr> <tr style="background-color: #fafafa; color: #999; height: 35px; margin: 0; padding: 0; text-align: center;"> <td style="margin: 0; padding: 0;">系统邮件，请勿直接回复。</td> </tr> </tbody> </table>';//邮件主体内容;

        if ($this->send_email($email, $subject, $content, $attachment = null)) {
            return true;
        } else {
            return false;
        }
    }
}